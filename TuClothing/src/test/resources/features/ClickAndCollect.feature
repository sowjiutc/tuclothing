Feature: click and collect

Scenario: Verify the functionality after checkout when guest place an order with click and collect
Given I am in checkout page as a guest in click and collect
When select store and proceed to payment page
Then User will be routed to payment gateway page to proceed with payment

