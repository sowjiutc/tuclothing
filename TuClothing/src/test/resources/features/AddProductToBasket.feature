Feature: Add product to basket

Scenario: Validate the add basket functionality with login credentials
Given User on product details page
When User entered valid data
And select product and add to basket
Then User  allowed to add item to basket

Scenario: Validate add basket functionality without login credentials
Given User on product details page
When User selected a product and add to basket
Then User is allowed without login to add items to basket

Scenario: validate functionality of multiple items to add in Basket with out login
Given User on product details page
When User selected multiple products and add to basket
Then User is allowed without login to add multiple items to basket

Scenario: validate functionality of multiple items to add in Basket with login credentials
Given User on product details page
When User login and select multiple products and add to basket
Then User allowed to add products to basket 

Scenario: Validate the functionality of guest removing the items from Basket
Given User on product details page
When User select to remove products from basket 
Then product should be removed from the basket

Scenario: Validate size should be disabled in case non availability of stock
Given User on product details page
When there is no availability in size 
Then size icon button should be disabled 

Scenario: Validate number of items in the cart should not be greater than available stock
Given User on product details page
When User added a product to the basket
Then quantity should not be greater than the stock








