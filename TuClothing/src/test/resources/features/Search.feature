Feature: Search functionality
// i just practised for scenario outline and parameterised method did not made any changes to code)

Scenario Outline: Verify the functionality of search product name
Given User on home page
When search with "<keyword>" keyword
Then system should provide all valid products

Examples:
|keyword|
|jeans|
|137430191|
|pink|


Scenario: Validate the functionality of search product number
Given User on home page
When search with valid products number
Then User should get the relevant product

Scenario: Validate the functionality of search product colour
Given User on home page
When search with valid products colour
Then system should provide all valid colour products

Scenario: Validate the blank search functionality
Given User on home page
When User provides blank search box 
Then system should popup with an error message

Scenario: Validate the auto complete list functionality
Given User on home page
When User provides product name in search box
Then system should display auto complete list of products with same category

Scenario: Verify the search functionality with special characters
Given User on home page
When User provide special characters in search box
Then all the products displayed on the page

Scenario: Verify the functionality of invalid data
Given User on home page
When search with invalid products data
Then system should display the error message of invalid data






