Feature: Basket checkout functionality

Scenario: Validate the update functionality in check out basket
Given User on checkout page
When User  increase or decrease the quantity
Then User allowed to click and update the items

Scenario: Validate the functionality of remove items in checkout basket
Given I am in basket page
When I am in checkout basket page 
Then the items can be removed from check out box

Scenario: Validate the functionalities of in valid voucher pay mode
Given I am in basket page
When  invalid  voucher added  to the checkout basket
Then the discount is not applicable

Scenario: Validate the functionalities of valid voucher pay mode
Given I am in basket page
When I added valid voucher to the check out basket
Then  discount should be accepted


Scenario: Validate the functionalities to decrease in quantities of the product
Given User on basket page
When User can decrease the number of items in basket page
Then User quantity is updated in checkout page

Scenario: Validate the functionalities to increase in quantities of the product when you check out
Given User on basket page
When User on basket page can increase the number of items
Then User increased quantity is updated in check out page


Scenario: Verify the maximum number of items to add in basket
Given User on basket page
When User can add maximum number of products
Then maximum products are added to checkout basket


Scenario: Validate the navigation of  checkout 
Given I am in basket page
When User added products to basket and checkout
Then the checkout page will navigate user to delivery option page


Scenario: Validate the functionalities of shopping items in mini cart can be viewed on the checkout page
Given User on basket page
When User added products to basket 
Then mini basket cart  is allowed to click and view the items



