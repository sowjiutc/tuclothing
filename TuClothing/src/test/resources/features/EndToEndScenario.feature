Feature: end to end scenario

Scenario: Verify the functionality when guest place an order with click and collect
Given I am in click and collect checkout page as a guest
When I select nearby store and proceed to summary page
Then I am routed to payment gateway page to proceed with payment

Scenario: Verify the functionality when guest place an order with Home delivery
Given I am in home delivery checkout page as a guest
When I select standard delivery and continue payment through card
Then User payment is successful 
