Feature: Store locator

Scenario: Verify the functionality of invalid post code
Given User on store locator page
When User entered invalid postcode 
Then error assigned sorry no result found

Scenario: Verify blank postcode when it is clicked find stores
Given User on store locator page
When User postcode field left blank
Then error assigned please fill the details

Scenario: Validate the functionality when click on the map is toggled full screen 
Given User on store locator page
When given valid postcode and search location
Then click on toggle out button on map
And system is allowed to toggleout

Scenario: Validate switching the view fuctionality maps to satellite
Given User on store locator page
When User click on map to satellite view
Then system should allow you to switch from maps to satellite
  
Scenario: Verify children clothing stores for click n collect  
Given User on store locator page
When given valid postcode
And click childrens clothing option
Then search display childrens clothing stores for click and collect

Scenario: Verify women clothing stores for click n collect
Given User on store locator page
When given valid postcode
And click women clothing option
Then search display women clothing stores for click and collect

Scenario: Verify men clothing stores for click n collect
Given User on store locator page
When given valid postcode
And click men clothing option
Then search display men clothing stores for click and collect

Scenario: Verify the functionality of post code with out checkboxes
Given User on store locator page
When User enter postcode on respective field
Then stores nearby are displayed

Scenario: Verify the store locator navigation 
Given User on home page
When User click store locator link
Then store locator page will be displayed

Scenario: Validate the functionality of valid postcode with click and collect.
Given User on store locator page
When provide valid postcode
And search is done to check nearest click and collect stores
Then system displayed all near by click and collect stores

Scenario: Verify children clothing stores for the valid post code
Given User on store locator page
When click children check box with valid postcode
And search with find stores
Then search will display children clothing stores

Scenario: Verify men clothing stores for the valid post code
Given User on store locator page
When select men clothing with a valid postcode
And search with find stores
Then search display men clothing stores

Scenario: Verify women clothing stores for the valid post code
Given User on store locator page
When select women clothing with a valid postcode
And search with find stores
Then search display women clothing stores

Scenario: Validate the functionality of zoom in to find stores in map
Given User on store locator page
When provide valid postcode 
And search is done to check the nearest stores
Then User will be able to zoom in the exact store address on map

Scenario: Validate the functionality of zoom out in map
Given User on store locator page
When provide valid postcode 
And search is done to check with the nearest stores
Then User will be able to zoom out  on map


