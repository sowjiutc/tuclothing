package com.stepDefinitions;

import org.junit.Assert;
import org.openqa.selenium.By;

import com.runner.BaseClass;

import cucumber.api.java.en.Then;

public class SearchResultsStepDef extends BaseClass {
	
	
	@Then("^system should provide all valid products$")
	public void system_should_provide_all_valid_products() throws Throwable {
		searchResultPage.searchResultPageForValidProducts();
	}
	@Then("^User should get the relevant product$")
	public void user_should_get_the_relevant_product() throws Throwable {
		searchResultPage.searchResultForValidProductNumber();
	}
	@Then("^system should provide all valid colour products$")
	public void system_should_provide_all_valid_colour_products() throws Throwable {
		searchResultPage.searchResultForValidProductColour();
	}
	@Then("^all the products displayed on the page$")
	public void all_the_products_displayed_on_the_page() throws Throwable {
		searchResultPage.searchWithSpecialAssert();
	}

}
