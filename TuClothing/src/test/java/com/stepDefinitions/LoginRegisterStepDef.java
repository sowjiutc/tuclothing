package com.stepDefinitions;

import java.util.Map;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.Select;

import com.runner.BaseClass;

import cucumber.api.DataTable;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class LoginRegisterStepDef extends BaseClass {
	

//	@When("^User enter valid credentials$")
//	public void user_enter_valid_credentials() throws Throwable {
//		loginRegisterPage.user_enter_valid_credentials();
//	}
	@When("^User enter valid credentials$")   
	public void user_enter_valid_credentials(DataTable userDetails) throws Throwable {
		 Map<String, String> loginDetails = userDetails.asMap(String.class, String.class);
	   loginRegisterPage.user_enter_valid_credentials(loginDetails.get("email"), loginDetails.get("password"));
	}
	
	@Then("^User should able to see welcome message$")
	public void user_should_able_to_see_welcome_message() throws Throwable {
		loginRegisterPage.UserEnterValidCredentialsAssert();
	}

	@When("^User provides invalid email id and password$")
	public void user_provides_invalid_email_id_and_password() throws Throwable {
		loginRegisterPage.user_provides_invalid_email_id_and_password();
		
	}
	@Then("^User should not able to login successfully with valid email id and invalid password$")
	public void user_should_not_able_to_login_successfully_with_valid_email_id_and_invalid_password() throws Throwable {
		loginRegisterPage.userWillGetErrorMessageAssert();
}
	@When("^User provides blank email id and password$")
	public void user_provides_blank_email_id_and_password() throws Throwable {
		loginRegisterPage.user_provides_blank_email_id_and_password();
	}
	@Then("^User should see error popup$")
	public void user_should_see_error_popup() throws Throwable {
		loginRegisterPage.usershouldseeerrorAssert();
	}
	@When("^User give invalid ID and valid password$")
	public void user_give_invalid_ID_and_valid_password() throws Throwable {
		loginRegisterPage.invalidIdAndValidPassword();
	}

	@Then("^User should get a error message with invalid Id and valid password$")
	public void user_should_get_a_error_message_with_invalid_Id_and_valid_password() throws Throwable {
		loginRegisterPage.invalidIdAndPasswordAssert();
	}
	@When("^User give invalid ID and Invalid password$")
	public void user_give_invalid_ID_and_Invalid_password() throws Throwable {
		loginRegisterPage.invalidIdAndInvalidPassword();
	}
	@Then("^User should get a error message please check the fields below and try again$")
	public void user_should_get_a_error_message_please_check_the_fields_below_and_try_again() throws Throwable {
		loginRegisterPage.invalidIdInvalidPasswordAssert();
	}
	@When("^User give valid credentials and click on hide or show password$")
	public void user_give_valid_credentials_and_click_on_hide_or_show_password() throws Throwable {
		loginRegisterPage.hideShowPassword();
	}
	@Then("^User should see the password which are in bullet points$")
	public void user_should_see_the_password_which_are_in_bullet_points() throws Throwable {
		loginRegisterPage.hideShowPasswordAssert();
	}
	@When("^User login with valid credentials and logout$")
	public void user_login_with_valid_credentials_and_logout() throws Throwable {
		loginRegisterPage.LoginWithValidCredentialsAndLogout();
	}
	@Then("^User should not be able to login back when click back button$")
	public void user_should_not_be_able_to_login_back_when_click_back_button() throws Throwable {
		loginRegisterPage.loginNLogOutClickBackButton();
	}
	@When("^User given valid credentials with out recaptcha$")
	public void user_given_valid_credentials_with_out_recaptcha() throws Throwable {
		loginRegisterPage.loginWithOutRecaptcha();
			}
	@Then("^User should not be able to log in with out recaptcha$")
	public void user_should_not_be_able_to_log_in_with_out_recaptcha() throws Throwable {
		loginRegisterPage.loginWithOutRecaptchaAssert();

	}
	@Then("^home page navigated to tu registration page$")
	public void home_page_navigated_to_tu_registration_page() throws Throwable {
		loginRegisterPage.NavigateToRegistrationPage();
	}
	@When("^Mandatory fields left empty and tried to register$")
	public void mandatory_fields_left_empty_and_tried_to_register() throws Throwable {
		loginRegisterPage.mandatoryFieldsEmptyAndRegister();
	}
	
	@Then("^error displayed$")
	public void error_displayed() throws Throwable {
		loginRegisterPage.mandatoryFieldsEmptyAndRegisterAssert();
	}
	@When("^User tried to register with out email id$")
	public void user_tried_to_register_with_out_email_id() throws Throwable {
		loginRegisterPage.registerWithOutEmail();
	}
	
	@Then("^an assigned error message displayed$")
	public void an_assigned_error_message_displayed() throws Throwable {
		loginRegisterPage.registerWithOutEmailAssert();
	}
	

	@When("^User tried to register invalid email id$")
	public void user_tried_to_register_invalid_email_id() throws Throwable {
		loginRegisterPage.registerWiThInvalidEmail();
			}
	
	@Then("^User can see an assigned error message$")
	public void user_can_see_an_assigned_error_message() throws Throwable {
		loginRegisterPage.registerWiThInvalidEmailAssert();
	}
	@When("^User tried to register with special characters and numbers$")
	public void user_tried_to_register_with_special_characters_and_numbers() throws Throwable {
		loginRegisterPage.registerWithSplCharectersAndNumbers();
	}
	@Then("^User will be seeing a error message$")
	public void user_will_be_seeing_a_error_message() throws Throwable {
		loginRegisterPage.mandatoryFieldsEmptyAndRegisterAssert();
			}
	@When("^password and confirm password are given different$")
	public void password_and_confirm_password_are_given_different() throws Throwable {
		loginRegisterPage.registerWithDiffPasswords();
	}
	@Then("^error message popup password do not match$")
	public void error_message_popup_password_do_not_match() throws Throwable {
		loginRegisterPage.registerWithDiffPasswordsAssert();
	}
	@When("^tried to register with minimum words$")
	public void tried_to_register_with_minimum_words() throws Throwable {
		loginRegisterPage.registerpasswordWithMinimumlength();
	}
	@Then("^error message pop up$")
	public void error_message_pop_up() throws Throwable {
		loginRegisterPage.registerpasswordWithMinimumlengthAssert();
	}
	@When("^User nectar card not mandatory$")
	public void user_nectar_card_not_mandatory() throws Throwable {
		loginRegisterPage.nectarCardNotMandatory();
	}
	@Then("^User can register his email$")
	public void user_can_register_his_email() throws Throwable {
		loginRegisterPage.nectarCardNotMandatoryAssert();
	}
	@When("^terms and conditions are not accepted$")
	public void terms_and_conditions_are_not_accepted() throws Throwable {
		loginRegisterPage.registerWithOutTermsAndConditions();
	}

	@Then("^User cannot register$")
	public void user_cannot_register() throws Throwable {
		loginRegisterPage.registerWithOutTermsAndConditionsAssert();
	}

//	@When("^image captcha button is not clicked$")
//	public void image_captcha_button_is_not_clicked() throws Throwable {
//		loginRegisterPage.registerWithOutReCaptcha();
//	}
	@When("^User enter valid credentials and image captcha button is not clicked$")
	public void user_enter_valid_credentials_and_image_captcha_button_is_not_clicked(DataTable userDetails) throws Throwable {
		loginRegisterPage.registerWithOutReCaptcha(userDetails);
		
	}

	
	@Then("^User cannot register with the given details$")
	public void user_cannot_register_with_the_given_details() throws Throwable {
		loginRegisterPage.registerWithOutReCaptchaAssert();
	}

	@When("^User tried to click on hide/show password$")
	public void user_tried_to_click_on_hide_show_password() throws Throwable {
		loginRegisterPage.clickHideShowButtonWhenRegister();
	}
	
	@Then("^User should see the password which are in stars$")
	public void user_should_see_the_password_which_are_in_stars() throws Throwable {
		loginRegisterPage.clickHideShowButtonWhenRegisterAssert();
	}
	@When("^User given all mandatory required details$")
	public void user_given_all_mandatory_required_details() throws Throwable {
		loginRegisterPage.registerWithNectarCardDetails();
	}
	@Then("^User can register with his email id and nectar card details$")
	public void user_can_register_with_his_email_id_and_nectar_card_details() throws Throwable {
		loginRegisterPage.registerWithNectarCardDetailsAssert();
	}
	
}

