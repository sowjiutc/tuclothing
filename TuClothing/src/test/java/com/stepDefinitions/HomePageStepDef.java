package com.stepDefinitions;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import com.runner.BaseClass;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class HomePageStepDef extends BaseClass {
	
	
	@Given("^User on home page$")
	public void user_on_home_page() throws Throwable {
		homePage.verifyHomePage();
	}

	@When("^search with \"([^\"]*)\" keyword$")
	public void search_with_keyword(String searchWord) throws Throwable {
		homePage.searchForvalidProductName(searchWord);
	}
	@When("^search with valid products number$")
	public void search_with_valid_products_number() throws Throwable {
		homePage.searchForValidProductNumber();
	}
	@When("^search with valid products colour$")
	public void search_with_valid_products_colour() throws Throwable {
		homePage.searchForValidProductColour();
	}
	@When("^User provides blank search box$")
	public void user_provides_blank_search_box() throws Throwable {	
		homePage.searchWithBlank();
	}
	@Then("^system should popup with an error message$")
	public void system_should_popup_with_an_error_message() throws Throwable {
		homePage.searchWithBlankAssert();
	}
	@When("^User provides product name in search box$")
	public void user_provides_product_name_in_search_box() throws Throwable {
		homePage.searchWithAutoCompleteList();
	}
	@Then("^system should display auto complete list of products with same category$")
	public void system_should_display_auto_complete_list_of_products_with_same_category() throws Throwable {
		homePage.searchWithAutoCompleteAssert();
	}
	@When("^User provide special characters in search box$")
	public void user_provide_special_characters_in_search_box() throws Throwable {
		homePage.searchWithSpecialCharecters();
	}
	@When("^search with invalid products data$")
	public void search_with_invalid_products_data() throws Throwable {
		homePage.searchWithInValidData();
	}
	@Then("^system should display the error message of invalid data$")
	public void system_should_display_the_error_message_of_invalid_data() throws Throwable {
		homePage.searchWithInValidAssert();
	}
	@When("^User click on login/register button$")
	public void user_click_on_login_register_button() throws Throwable {
		homePage.validLoginCredentials();
			}
	@When("^User click on login/registration link$")
	public void user_click_on_login_registration_link() throws Throwable {
		homePage.loginNRegistrationLink();
		
	}

}