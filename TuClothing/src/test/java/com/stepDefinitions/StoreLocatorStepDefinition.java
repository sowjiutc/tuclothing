package com.stepDefinitions;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.chrome.ChromeDriver;

import com.runner.BaseClass;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class StoreLocatorStepDefinition extends BaseClass {
	
	@Given("^User on store locator page$")
	public void user_on_store_locator_page() throws Throwable {
		storeLocatorPage.verifyStoreLocatorPage();
	}
	
	@When("^User entered invalid postcode$")
	public void user_entered_invalid_postcode() throws Throwable {
		storeLocatorPage.invalidPostCode();
	}
	@Then("^error assigned sorry no result found$")
	public void error_assigned_sorry_no_result_found() throws Throwable {
		storeLocatorPage.invalidPostCodeAssert();
	}
	@When("^User postcode field left blank$")
	public void user_postcode_field_left_blank() throws Throwable {
		storeLocatorPage.verifyBlankPostCode();
	}
	@Then("^error assigned please fill the details$")
	public void error_assigned_please_fill_the_details() throws Throwable {
		storeLocatorPage.verifyBlankPostCodeAssert();
	}
	@When("^given valid postcode and search location$")
	public void given_valid_postcode_and_search_location() throws Throwable {
		storeLocatorPage.toggleFullScreen();
	}
	@Then("^click on toggle out button on map$")
	public void click_on_toggle_out_button_on_map() throws Throwable {
		storeLocatorPage.toggleFullScreenOnMap();
	}
	@Then("^system is allowed to toggleout$")
	public void system_is_allowed_to_toggleout() throws Throwable {
		storeLocatorPage.toggleFullScreenOnMapAssert();
	}
	@When("^User click on map to satellite view$")
	public void user_click_on_map_to_satellite_view() throws Throwable {
		storeLocatorPage.SwitchViewMapToSatellite();
	}
	@Then("^system should allow you to switch from maps to satellite$")
	public void system_should_allow_you_to_switch_from_maps_to_satellite() throws Throwable {
		storeLocatorPage.SwitchViewMapToSatelliteAssert();
	}
	@When("^given valid postcode$")
	public void given_valid_postcode() throws Throwable {
		storeLocatorPage.clickAndCollectChildrenWithValidPostcode();
	}
	
	@When("^click childrens clothing option$")
	public void click_childrens_clothing_option() throws Throwable {
		storeLocatorPage.clickAndCollectChildren();
	}
	
	@Then("^search display childrens clothing stores for click and collect$")
	public void search_display_childrens_clothing_stores_for_click_and_collect() throws Throwable {
		storeLocatorPage.clickAndCollectChildrenAssert();
	}	
	
	@When("^click women clothing option$")
	public void click_women_clothing_option() throws Throwable {
		storeLocatorPage.clickAndCollectWomen();
	}
	@Then("^search display women clothing stores for click and collect$")
	public void search_display_women_clothing_stores_for_click_and_collect() throws Throwable {
		storeLocatorPage.clickAndCollectChildrenAssert();
	}
	@When("^click men clothing option$")
	public void click_men_clothing_option() throws Throwable {
		storeLocatorPage.clickAndCollectMen();
	}
	@Then("^search display men clothing stores for click and collect$")
	public void search_display_men_clothing_stores_for_click_and_collect() throws Throwable {
		storeLocatorPage.clickAndCollectMenAssert();
	}
	@When("^User enter postcode on respective field$")
	public void user_enter_postcode_on_respective_field() throws Throwable {
		storeLocatorPage.postcodeWithOutCheckBoxes();
	}
	@Then("^stores nearby are displayed$")
	public void stores_nearby_are_displayed() throws Throwable {
		storeLocatorPage.postcodeWithOutCheckBoxesAssert();
	}
	@When("^User click store locator link$")
	public void user_click_store_locator_link() throws Throwable {
		storeLocatorPage.storeLocatorNavigation();
	}
	@Then("^store locator page will be displayed$")
	public void store_locator_page_will_be_displayed() throws Throwable {
		storeLocatorPage.storeLocatorNavigationAssert();
	}
	@When("^provide valid postcode$")
	public void provide_valid_postcode() throws Throwable {
		storeLocatorPage.validPostCodeCNC();
	}
	@When("^search is done to check nearest click and collect stores$")
	public void search_is_done_to_check_nearest_click_and_collect_stores() throws Throwable {
		storeLocatorPage.ValidPostCodeNearbyCNC();
	}
	@Then("^system displayed all near by click and collect stores$")
	public void system_displayed_all_near_by_click_and_collect_stores() throws Throwable {
		storeLocatorPage.validPostCodeCNCAssert();
	}
	@When("^click children check box with valid postcode$")
	public void click_children_check_box_with_valid_postcode() throws Throwable {
		storeLocatorPage.childrensClothStoreValidPostcode();
	}
	@When("^search with find stores$")
	public void search_with_find_stores() throws Throwable {
		storeLocatorPage.childrensClothStoreSearch();
	}
	@Then("^search will display children clothing stores$")
	public void search_will_display_children_clothing_stores() throws Throwable {
		storeLocatorPage.childrensClothStoresAssert();
	}
	@When("^select men clothing with a valid postcode$")
	public void select_men_clothing_with_a_valid_postcode() throws Throwable {
		storeLocatorPage.mensclothingStore();
	}
	@Then("^search display men clothing stores$")
	public void search_display_men_clothing_stores() throws Throwable {
		storeLocatorPage.mensClothinsAssert();
	}
	@When("^select women clothing with a valid postcode$")
	public void select_women_clothing_with_a_valid_postcode() throws Throwable {
		storeLocatorPage.womensClothingStores();
	}
	@Then("^search display women clothing stores$")
	public void search_display_women_clothing_stores() throws Throwable {
		storeLocatorPage.womensClothingStoresAssert();
	}
	@When("^search is done to check the nearest stores$")
	public void search_is_done_to_check_the_nearest_stores() throws Throwable {
		storeLocatorPage.zoomIN();
	}
	@Then("^User will be able to zoom in the exact store address on map$")
	public void user_will_be_able_to_zoom_in_the_exact_store_address_on_map() throws Throwable {
		storeLocatorPage.zoomInAssert();
	}
	@When("^search is done to check with the nearest stores$")
	public void search_is_done_to_check_with_the_nearest_stores() throws Throwable {
		storeLocatorPage.zoomOut();
	}
	@Then("^User will be able to zoom out  on map$")
	public void user_will_be_able_to_zoom_out_on_map() throws Throwable {
		storeLocatorPage.zoomOutAssert();
	}
	}
	
	
	
	
	
