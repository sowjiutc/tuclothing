package com.stepDefinitions;

import com.runner.BaseClass;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class BasketStepDef extends BaseClass {

	@Given("^User on checkout page$")
	public void user_on_checkout_page() throws Throwable {
		basketPage.userOnCheckOutPage();
	}

	@When("^User  increase or decrease the quantity$")
	public void user_increase_or_decrease_the_quantity() throws Throwable {
		basketPage.increaseDecreaseQuanityUpdate();
	}

	@Then("^User allowed to click and update the items$")
	public void user_allowed_to_click_and_update_the_items() throws Throwable {
		basketPage.increaseDecreaseQuanityUpdateAssert();
	}

	@Given("^I am in basket page$")
	public void i_am_in_basket_page() throws Throwable {
		basketPage.userOnCheckOutPage();
	}

	@When("^I am in checkout basket page$")
	public void i_am_in_checkout_basket_page() throws Throwable {
		basketPage.removeAllItemsFromBasketInCheckoutPage();
	}

	@Then("^the items can be removed from check out box$")
	public void the_items_can_be_removed_from_check_out_box() throws Throwable {
		basketPage.removeAllItemsFromBasketInCheckoutPageAssert();
	}

	@When("^invalid  voucher added  to the checkout basket$")
	public void invalid_voucher_added_to_the_checkout_basket() throws Throwable {
		basketPage.invalidPayVoucher();
	}

	@Then("^the discount is not applicable$")
	public void the_discount_is_not_applicable() throws Throwable {
		basketPage.invalidPayVoucherAssert();

	}

	@When("^User can decrease the number of items in basket page$")
	public void user_can_decrease_the_number_of_items_in_basket_page() throws Throwable {
		basketPage.checkoutWithItemsDecreased();
	}

	@Then("^User quantity is updated in checkout page$")
	public void user_quantity_is_updated_in_checkout_page() throws Throwable {
		basketPage.checkoutWithItemsDecreasedAssert();
	}

	@When("^User on basket page can increase the number of items$")
	public void user_on_basket_page_can_increase_the_number_of_items() throws Throwable {
		basketPage.checkoutWithItemsIncreased();
	}

	@Then("^User increased quantity is updated in check out page$")
	public void user_increased_quantity_is_updated_in_check_out_page() throws Throwable {
		basketPage.checkoutWithItemsIncreasedAssert();
	}
	
	@When("^User can add maximum number of products$")
	public void user_can_add_maximum_number_of_products() throws Throwable {
		basketPage.addingMaxNumberOfProducts();
	}
	
	@Then("^maximum products are added to checkout basket$")
	public void maximum_products_are_added_to_checkout_basket() throws Throwable {
		basketPage.addingMaxNumberOfProductsAssert();
	}
	
	@When("^User added products to basket and checkout$")
	public void user_added_products_to_basket_and_checkout() throws Throwable {
		basketPage.validateNavigationOfCheckout();
	}
	@Then("^the checkout page will navigate user to delivery option page$")
	public void the_checkout_page_will_navigate_user_to_delivery_option_page() throws Throwable {
		basketPage.validateNavigationOfCheckoutAssert();
	}
	@When("^User added products to basket$")
	public void user_added_products_to_basket() throws Throwable {
		basketPage.checkoutMiniBasket();
	}
	@Then("^mini basket cart  is allowed to click and view the items$")
	public void mini_basket_cart_is_allowed_to_click_and_view_the_items() throws Throwable {
		basketPage.checkoutMiniBasketAssert();
	}

}
