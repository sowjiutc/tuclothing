package com.stepDefinitions;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.Select;

import com.runner.BaseClass;

import cucumber.api.java.en.Then;

public class PaymentPageStepDef extends BaseClass {

	@Then("^User will be routed to payment gateway page to proceed with payment$")
	public void user_will_be_routed_to_payment_gateway_page_to_proceed_with_payment() throws Throwable {
		paymentPage.verifyPaymentNavigationAssert();

	}

	@Then("^User payment will be sucessful$")
	public void user_payment_will_be_sucessful() throws Throwable {
		paymentPage.paymentCANDCProcessAssert();
	}

	@Then("^I am routed to payment gateway page to proceed with payment$")
	public void i_am_routed_to_payment_gateway_page_to_proceed_with_payment() throws Throwable {
		paymentPage.homeDeliveryPaymentProcess();
	}

	@Then("^User payment is successful$")
	public void user_payment_is_successful() throws Throwable {
		paymentPage.guestCheckOutEndToEndAssert();
	}

}
