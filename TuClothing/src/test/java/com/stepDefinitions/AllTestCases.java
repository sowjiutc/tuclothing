package com.stepDefinitions;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

import cucumber.api.java.After;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class AllTestCases {

	public static WebDriver driver;
	private static String baseURL = "https://tuclothing.sainsburys.co.uk/";
	// Search
	private static By SEARCH = By.cssSelector("#search");
	private static By SEARCHBUTTON = By.cssSelector(".searchButton");
	private static By ERRORMESSAGEFORBLANKDATA=By.cssSelector("#search-empty-errors"); //Assertion
	// Login
	private static By LOGINREGISTER = By.linkText("Tu Log In / Register");
	private static By EMAILID = By.cssSelector("#j_username");
	private static By PASSWORD = By.cssSelector("#j_password");
	private static By FORGOTPASSWORD = By.cssSelector("#forgot-password-link");
	private static By lOGINBUTTON = By.cssSelector("#submit-login");
	private static By HIDESHOW = By.cssSelector(".hide-show-button");
	// Register
	private static By REGISTERBUTTON = By.cssSelector(".regToggle");
	private static By REGISTEREMAIL = By.cssSelector("#register_email");
	private static By REGISTERTITLE = By.cssSelector("#register_title");
	private static By REGISTERFIRSTNAME = By.cssSelector("#register_firstName");
	private static By REGISTERLASTNAME = By.cssSelector("#register_lastName");
	private static By REGPASSWORD = By.cssSelector("#password");
	private static By REGCHECKPASSWORD = By.cssSelector("#register_checkPwd");
	private static By COMPLETEREGISTER = By.cssSelector("#submit-register");
	private static By REGISTERNECTARPOINTS = By.cssSelector("#regNectarPointsOne");
	private static By TERMSNCONDITIONS = By.linkText("terms and conditions");
	private static By SHOW = By.linkText("Show");
	// Store Locator
	private static By STORELOCATORlINK = By.linkText("Tu Store Locator");
	private static By POSTCODEBOX = By.cssSelector("input[placeholder='Postcode or town']");
	private static By FINDSTORES = By.cssSelector("button[class='ln-c-button ln-c-button--primary']");
	private static By TOGGLEFULLSCREEN = By.cssSelector("button[title='Toggle fullscreen view']");
	private static By SATELLITE = By.cssSelector("div[title='Show satellite imagery']");
	private static By MAP = By.cssSelector("div[title='Show street map']");
	private static By CHILDREN = By.cssSelector("label[for='children']");
	private static By CLICKANDCOLLECT = By.cssSelector("label[for='click']");
	private static By MENS = By.cssSelector("label[for='men']");
	private static By WOMENS = By.cssSelector("label[for='women']");
	private static By ZOOMIN = By.cssSelector("button[title='Zoom in']");
	private static By ZOOMOUT = By.cssSelector("button[title='Zoom out']");
	// Add prodcut to basket
	private static By WOMENLINK = By.linkText("Women");
	private static By NEWARRIVALS = By.linkText("New Arrivals");
	private static By SIZECHECKBOX = By.cssSelector(".selectable");
	private static By PRODUCTQUANTITYDROPDOWN = By.cssSelector("#productVariantQty");
	private static By ADDTOCART = By.cssSelector("#AddToCart");
	private static By ITEMIMAGESELECTOR = By.cssSelector(".image-component img");
	private static By MINIBASKET = By.cssSelector(".nav_cart_text");
	private static By MINIVIEWBASKET = By.cssSelector("a[href='/cart']");
	private static By VIEWBASKET = By.cssSelector("#basket-title");
	private static By REMOVEDCARTITEM = By.cssSelector(".removeCartItem");
	private static By CLICKVIEWBASKET = By.cssSelector("a[class='ln-c-button ln-c-button--primary']");
	private static By REMOVEPRODUCT = By.cssSelector("#RemoveProduct_0");
	private static By BASKETEMPTY = By.cssSelector(".cart_icon");
	private static By SIZEDISABLED = By.cssSelector("div[data-value='137621027']");
	private static By QUANTITYUPDATE = By.cssSelector("a[class='basketUpdateProd updateQuantityProduct']");
	//Basket CheckOut 
		private static By CHECKOUTBUTTON=By.cssSelector("a[data-testid='checkoutButton']");
		private static By SHOWPROMO=By.cssSelector("#showPromo");
		private static By VOUCHERPAY=By.cssSelector("#voucher_voucherCode");
		private static By VOUCHERAPPLY=By.cssSelector("button[class='ln-c-button ln-c-button--secondary']");
		private static By PROCEEDTOCHECKOUTUP=By.cssSelector("#basketButtonTop");
		private static By PROCEEDTOCHECKOUTDOWN=By.cssSelector("#basketButtonBottom");
		private static By GUESTEMAIL=By.cssSelector("#guest_email");
		private static By GUESTCHECKOUT=By.cssSelector("button[data-testid='guest_checkout']");  //guest checkout
		//Home Delivery
		private static By HOMEDELIVERY=By.cssSelector("label[for='HOME_DELIVERY']"); 
		private static By HDCONTINUE=By.cssSelector(".ln-c-button.ln-c-button--primary"); // Homedelivery continue button
		private static By HDFIRSTNAME=By.cssSelector("input[name='firstName']"); 
		private static By HDLASTNAME=By.cssSelector("input[name='lastName']"); 
		private static By HDPOSTCODE=By.cssSelector("#addressPostcode");  
		private static By HDFINDADRESS=By.cssSelector(".address-lookup.ln-u-push-bottom");
		private static By HDADRESSDROPDOWN=By.cssSelector("#addressListView");
		private static By HDBILLINGCHECKBOX=By.cssSelector(".ln-c-form-option__label");
		private static By HDADRESSCONTINUE=By.cssSelector("#continue");	
		private static By HDSTANDARDDELIVERY=By.cssSelector("label[for='standard-delivery']"); 
		private static By HDSDCONTINUE=By.cssSelector("input[data-testid='continue']");   //HOMEDELIVERY STANDARD DELIVERY CONTNIUE
		private static By HDCONTINUEPAYMENT=By.cssSelector("button[data-testid='continueToPayment']");
		private static By HDPAYWITHCARD =By.cssSelector("a[data-testid='payWithCard']");
		private static By HDTERMSANDCONDITIONS =By.cssSelector(".ln-c-form-option__label");
		private static By HDCARDPAYMENTCONTINUE=By.cssSelector("#contPayment");
		// Click and Collect
		private static By CNCDELIVERY=By.cssSelector("label[for='CLICK_AND_COLLECT']");   //CLICK N COLLECTs
		private static By CNCCONTINUE=By.cssSelector(".ln-c-button.ln-c-button--primary");
		private static By CNCLOOKUP=By.cssSelector("#lookup");
		private static By CNCLOOKUPBUTTON=By.cssSelector("span[data-testid='button-text']");
		private static By CNCSTORESELECT=By.cssSelector("button[data-testid='select-store']");
		private static By CNCSUMMARYBUTTON=By.cssSelector("button[data-testid='submit-button']");
		private static By CNCCONTINUEPAYMENT=By.cssSelector("button[data-testid='continueToPayment']");
		private static By CNCPAYWITHCARD=By.linkText("Pay with a card");
		private static By CNCTITLEDROPDOWN=By.cssSelector(".ln-c-form-group.ln-u-push-bottom .ln-c-select");
		private static By CNCFIRSTNAME=By.cssSelector("input[name='newFirstName']");
		private static By CNCSURNAME=By.cssSelector("input[name='newSurname']");
		private static By  CNCADRESSPOSTCODE=By.cssSelector("#addressPostcode");
		private static By  CNCFINDADRESS=By.cssSelector("button[class='ln-c-button ln-c-button--primary']");
		private static By CNCADRESSDROPDOWN=By.cssSelector("#addressListView");
		private static By CNCTERMSANDCONDITIONS=By.cssSelector(".ln-c-form-option__label");
		private static By CNCCARDPAYMENTCONTNIUE=By.cssSelector("#contPayment");
		
		
		

//	@Given("^User on home page$")
//	public void user_on_home_page() throws Throwable {
//		System.setProperty("webdriver.chrome.driver", "C:\\Selenium-API\\ChromeDriver\\chromedriver.exe");
//		driver = new ChromeDriver();
//		driver.get(baseURL);
//		driver.manage().window().maximize();
//		Assert.assertEquals("https://tuclothing.sainsburys.co.uk/", driver.getCurrentUrl());
//	}
//
//	@When("^search with valid products name$")
//	public void search_with_valid_products_name() throws Throwable {
//		driver.findElement(SEARCH).clear();
//		driver.findElement(SEARCH).sendKeys("jeans");
//		driver.findElement(SEARCHBUTTON).click();
//	}
//
//	@Then("^system should provide all valid products$")
//	public void system_should_provide_all_valid_products() throws Throwable {
//		Assert.assertTrue(driver.findElement(By.cssSelector("h1")).getText().toLowerCase().contains("jeans"));
//
//	}
//
//	@When("^search with valid products number$")
//	public void search_with_valid_products_number() throws Throwable {
//		driver.findElement(SEARCH).clear();
//		driver.findElement(SEARCH).sendKeys("137430191");
//		driver.findElement(SEARCHBUTTON).click();
//
//	}
//
//	@Then("^User should get the relevant product$")
//	public void user_should_get_the_relevant_product() throws Throwable {
//		Assert.assertTrue(driver.findElement(By.cssSelector("h1")).getText().toLowerCase().contains("'15487665'"));
//	}
//
//	@When("^search with valid products colour$")
//	public void search_with_valid_products_colour() throws Throwable {
//		driver.findElement(SEARCH).clear();
//		driver.findElement(SEARCH).sendKeys("pink");
//		driver.findElement(SEARCHBUTTON).click();
//	}
//
//	@Then("^system should provide all valid colour products$")
//	public void system_should_provide_all_valid_colour_products() throws Throwable {
//		Assert.assertTrue(driver.findElement(By.cssSelector("h1")).getText().toLowerCase().contains("pink"));
//	}
//
//	@When("^User provides blank search box$")
//	public void user_provides_blank_search_box() throws Throwable {
//		driver.findElement(SEARCH).clear();
//		driver.findElement(SEARCH).sendKeys("");
//		driver.findElement(SEARCHBUTTON).click();
//
//	}
//
//	@Then("^system should popup with an error message$")
//	public void system_should_popup_with_an_error_message() throws Throwable {
//		Assert.assertEquals("Please complete a product search",
//				driver.findElement(By.cssSelector("#search-empty-errors")).getText());
//	}
//
//	@When("^User provides product name in search box$")
//	public void user_provides_product_name_in_search_box() throws Throwable {
//		driver.findElement(SEARCH).clear();
//		driver.findElement(SEARCH).sendKeys("dungarees");
//	}
//
//	@Then("^system should display auto complete list of products with same category$")
//	public void system_should_display_auto_complete_list_of_products_with_same_category() throws Throwable {
//		Assert.assertEquals("https://tuclothing.sainsburys.co.uk/", driver.getCurrentUrl());
//	}
//
//	@When("^User provide special characters in search box$")
//	public void user_provide_special_characters_in_search_box() throws Throwable {
//		driver.findElement(SEARCH).clear();
//		driver.findElement(SEARCH).sendKeys("@\\!&");
//		driver.findElement(SEARCHBUTTON).click();
//	}
//
//	@Then("^all the products displayed on the page$")
//	public void all_the_products_displayed_on_the_page() throws Throwable {
//		Assert.assertTrue(driver.findElement(By.cssSelector("h1")).getText().contains("@\\!&"));
//	}
//
//	@When("^search with invalid products data$")
//	public void search_with_invalid_products_data() throws Throwable {
//		driver.findElement(SEARCH).clear();
//		driver.findElement(SEARCH).sendKeys("cheetos");
//		driver.findElement(SEARCHBUTTON).click();
//	}
//
//	@Then("^system should display the error message of invalid data$")
//	public void system_should_display_the_error_message_of_invalid_data() throws Throwable {
//		Assert.assertTrue(driver.findElement(By.cssSelector("h1")).getText().contains("'cheetos'"));
//		System.out.println("Sorry, no results for 'cheetos' ");
//	}
//
//	// Login
//
//	@When("^User click on login/register button$")
//	public void user_click_on_login_register_button() throws Throwable {
//		driver.findElement(LOGINREGISTER).click();
//		
//			}
//
//	@When("^User enter valid credentials$")
//	public void user_enter_valid_credentials() throws Throwable {
//		driver.findElement(EMAILID).clear();
//		driver.findElement(EMAILID).sendKeys("Sunny@gmail.com");
//		driver.findElement(PASSWORD).clear();
//		driver.findElement(PASSWORD).sendKeys("sunshine1234");
//
//	}
//
//	@Then("^User should able to see welcome message$")
//	public void user_should_able_to_see_welcome_message() throws Throwable {
//		Assert.assertEquals("Log in with your Tu details",
//				driver.findElement(By.cssSelector("#login-register-form .span-12.retuncustomer .loginFormHolder h3"))
//						.getText());
//		System.out.println("Please check Recaptcha");
//	}
//
//	@When("^User provides invalid email id and password$")
//	public void user_provides_invalid_email_id_and_password() throws Throwable {
//		driver.findElement(LOGINREGISTER).click();
//		driver.findElement(EMAILID).clear();
//		driver.findElement(EMAILID).sendKeys("sdgfagd@gmail.com");
//		driver.findElement(PASSWORD).clear();
//		driver.findElement(PASSWORD).sendKeys("asdjgashkdSGK");
//	}
//
//	@Then("^User should not able to login successfully with valid email id and invalid password$")
//	public void user_should_not_able_to_login_successfully_with_valid_email_id_and_invalid_password() throws Throwable {
//		Assert.assertEquals("Log in with your Tu details",
//				driver.findElement(By.cssSelector("#login-register-form .span-12.retuncustomer .loginFormHolder h3"))
//						.getText());
//		System.out.println("Please check Recaptcha");
//	}
//
//	@When("^User provides blank email id and password$")
//	public void user_provides_blank_email_id_and_password() throws Throwable {
//		driver.findElement(LOGINREGISTER).click();
//		driver.findElement(EMAILID).clear();
//		driver.findElement(EMAILID).sendKeys("");
//		driver.findElement(PASSWORD).clear();
//		driver.findElement(PASSWORD).sendKeys("");
//	}
//
//	@Then("^User should see error popup$")
//	public void user_should_see_error_popup() throws Throwable {
//		Assert.assertEquals("Log in with your Tu details",
//				driver.findElement(By.cssSelector("#login-register-form .span-12.retuncustomer .loginFormHolder h3"))
//						.getText());
//		System.out.println("Please check Recaptcha");
//	}
//
//	@When("^User give invalid ID and valid password$")
//	public void user_give_invalid_ID_and_valid_password() throws Throwable {
//		driver.findElement(LOGINREGISTER).click();
//		driver.findElement(EMAILID).clear();
//		driver.findElement(EMAILID).sendKeys("dfgadhfgad@gmail.com ");
//		driver.findElement(PASSWORD).clear();
//		driver.findElement(PASSWORD).sendKeys("sunshine1234");
//	}
//
//	@Then("^User should get a error message with invalid Id and valid password$")
//	public void user_should_get_a_error_message_with_invalid_Id_and_valid_password() throws Throwable {
//		Assert.assertEquals("Log in with your Tu details",
//				driver.findElement(By.cssSelector("#login-register-form .span-12.retuncustomer .loginFormHolder h3"))
//						.getText());
//		System.out.println("Please check Recaptcha");
//	}
//
//	@When("^User give invalid ID and Invalid password$")
//	public void user_give_invalid_ID_and_Invalid_password() throws Throwable {
//		driver.findElement(LOGINREGISTER).click();
//		driver.findElement(EMAILID).clear();
//		driver.findElement(EMAILID).sendKeys("FBAfjAFJ@gmail.com");
//		driver.findElement(PASSWORD).clear();
//		driver.findElement(PASSWORD).sendKeys("sadvasmbv");
//	}
//
//	@Then("^User should get a error message please check the fields below and try again$")
//	public void user_should_get_a_error_message_please_check_the_fields_below_and_try_again() throws Throwable {
//		Assert.assertEquals("Log in with your Tu details",
//				driver.findElement(By.cssSelector("#login-register-form .span-12.retuncustomer .loginFormHolder h3"))
//						.getText());
//		System.out.println("please check the fields below and try again");
//	}
//
//	@When("^User give valid credentials and click on hide or show password$")
//	public void user_give_valid_credentials_and_click_on_hide_or_show_password() throws Throwable {
//		driver.findElement(LOGINREGISTER).click();
//		driver.findElement(EMAILID).clear();
//		driver.findElement(EMAILID).sendKeys("Sunny@gmail.com");
//		driver.findElement(PASSWORD).clear();
//		driver.findElement(PASSWORD).sendKeys("sunshine1234");
//		Thread.sleep(3000);
//		driver.findElement(HIDESHOW).click();
//	}
//
//	@Then("^User should see the password which are in bullet points$")
//	public void user_should_see_the_password_which_are_in_bullet_points() throws Throwable {
//		Assert.assertEquals("Log in with your Tu details",
//				driver.findElement(By.cssSelector("#login-register-form .span-12.retuncustomer .loginFormHolder h3"))
//						.getText());
//
//	}
//
//	@When("^User login with valid credentials and logout$")
//	public void user_login_with_valid_credentials_and_logout() throws Throwable {
//		driver.findElement(LOGINREGISTER).click();
//		driver.findElement(EMAILID).clear();
//		driver.findElement(EMAILID).sendKeys("Sunny@gmail.com");
//		driver.findElement(PASSWORD).clear();
//		driver.findElement(PASSWORD).sendKeys("sunshine1234");
//
//	}
//
//	@Then("^User should not be able to login back when click back button$")
//	public void user_should_not_be_able_to_login_back_when_click_back_button() throws Throwable {
//		Assert.assertEquals("Log in with your Tu details",
//				driver.findElement(By.cssSelector("#login-register-form .span-12.retuncustomer .loginFormHolder h3"))
//						.getText());
//	}
//
//	@When("^User given valid credentials with out recaptcha$")
//	public void user_given_valid_credentials_with_out_recaptcha() throws Throwable {
//		driver.findElement(LOGINREGISTER).click();
//		driver.findElement(EMAILID).clear();
//		driver.findElement(EMAILID).sendKeys("Sunny@gmail.com");
//		driver.findElement(PASSWORD).clear();
//		driver.findElement(PASSWORD).sendKeys("sunshine1234");
//
//	}
//
//	@Then("^User should not be able to log in with out recaptcha$")
//	public void user_should_not_be_able_to_log_in_with_out_recaptcha() throws Throwable {
//		Assert.assertEquals("Log in with your Tu details",
//				driver.findElement(By.cssSelector("#login-register-form .span-12.retuncustomer .loginFormHolder h3"))
//						.getText());
//
//	}
//
//	// Register
//
//	@When("^User click on login/registration link$")
//	public void user_click_on_login_registration_link() throws Throwable {
//		driver.findElement(LOGINREGISTER).click();
//		
//	}
//
//	@Then("^home page navigated to tu registration page$")
//	public void home_page_navigated_to_tu_registration_page() throws Throwable {
//		Thread.sleep(3000);
//		driver.findElement(REGISTERBUTTON).click();
//		Assert.assertEquals("Register with Tu", driver.findElement(By.cssSelector(".span-12.last h2")).getText());
//	}
//
//	@When("^Mandatory fields left empty and tried to register$")
//	public void mandatory_fields_left_empty_and_tried_to_register() throws Throwable {
//		driver.findElement(LOGINREGISTER).click();				//  Duplicate entry in HomePage 
//		driver.findElement(REGISTERBUTTON).click(); // retoggle register
//		driver.findElement(REGISTEREMAIL).clear();
//		driver.findElement(REGISTEREMAIL).sendKeys(" ");
//		Select titleDropDown = new Select(driver.findElement(REGISTERTITLE));
//		titleDropDown.selectByIndex(3);
//		driver.findElement(REGISTERFIRSTNAME).clear();
//		driver.findElement(REGISTERFIRSTNAME).click();
//		driver.findElement(REGISTERFIRSTNAME).sendKeys("");
//		driver.findElement(REGISTERLASTNAME).clear();
//		driver.findElement(REGISTERLASTNAME).click();
//		driver.findElement(REGISTERLASTNAME).sendKeys("");
//		driver.findElement(REGPASSWORD).clear();
//		driver.findElement(REGPASSWORD).click();
//		driver.findElement(REGPASSWORD).sendKeys("");
//		driver.findElement(REGCHECKPASSWORD).clear();
//		driver.findElement(REGCHECKPASSWORD).click();
//		driver.findElement(REGCHECKPASSWORD).sendKeys("");
//		driver.findElement(REGISTERNECTARPOINTS).click();
//		driver.findElement(REGISTERNECTARPOINTS).sendKeys("");
//		driver.findElement(COMPLETEREGISTER).click();
//
//	}
//
//	@Then("^error displayed$")
//	public void error_displayed() throws Throwable {
//		Assert.assertEquals("Register with Tu", driver.findElement(By.cssSelector(".span-12.last h2")).getText());
//		System.out.println("please fill the blanks");
//	}
//
//	@When("^User tried to register with out email id$")
//	public void user_tried_to_register_with_out_email_id() throws Throwable {
//		driver.findElement(LOGINREGISTER).click();
//		driver.findElement(REGISTERBUTTON).click();
//		driver.findElement(REGISTEREMAIL).clear();
//		driver.findElement(REGISTEREMAIL).sendKeys("    ");
//		Select titleDropDown = new Select(driver.findElement(REGISTERTITLE));
//		titleDropDown.selectByIndex(3);
//		driver.findElement(REGISTERFIRSTNAME).clear();
//		driver.findElement(REGISTERFIRSTNAME).click();
//		driver.findElement(REGISTERFIRSTNAME).sendKeys("veena");
//		driver.findElement(REGISTERLASTNAME).clear();
//		driver.findElement(REGISTERLASTNAME).click();
//		driver.findElement(REGISTERLASTNAME).sendKeys("gokula");
//		driver.findElement(REGPASSWORD).clear();
//		driver.findElement(REGPASSWORD).click();
//		driver.findElement(REGPASSWORD).sendKeys("man@1234");
//		driver.findElement(REGCHECKPASSWORD).clear();
//		driver.findElement(REGCHECKPASSWORD).click();
//		driver.findElement(REGCHECKPASSWORD).sendKeys("man@1234");
//		driver.findElement(REGISTERNECTARPOINTS).click();
//		driver.findElement(REGISTERNECTARPOINTS).sendKeys("12345678");
//		driver.findElement(COMPLETEREGISTER).click();
//	}
//
//	@Then("^an assigned error message displayed$")
//	public void an_assigned_error_message_displayed() throws Throwable {
//		Assert.assertEquals("Register with Tu", driver.findElement(By.cssSelector(".span-12.last h2")).getText());
//		System.out.println("please checkRecaptcha");
//	}
//
//	@When("^User tried to register invalid email id$")
//	public void user_tried_to_register_invalid_email_id() throws Throwable {
//		driver.findElement(LOGINREGISTER).click();    
//		driver.findElement(REGISTERBUTTON).click();
//		driver.findElement(REGISTEREMAIL).clear();
//		driver.findElement(REGISTEREMAIL).sendKeys("veenagokulagmail.com");
//		Select titleDropDown = new Select(driver.findElement(REGISTERTITLE));
//		titleDropDown.selectByIndex(3);
//		driver.findElement(REGISTERFIRSTNAME).clear();
//		driver.findElement(REGISTERFIRSTNAME).click();
//		driver.findElement(REGISTERFIRSTNAME).sendKeys("veena");
//		driver.findElement(REGISTERLASTNAME).clear();
//		driver.findElement(REGISTERLASTNAME).click();
//		driver.findElement(REGISTERLASTNAME).sendKeys("gokula");
//		driver.findElement(REGPASSWORD).clear();
//		driver.findElement(REGPASSWORD).click();
//		driver.findElement(REGPASSWORD).sendKeys("man@1234");
//		driver.findElement(REGCHECKPASSWORD).clear();
//		driver.findElement(REGCHECKPASSWORD).click();
//		driver.findElement(REGCHECKPASSWORD).sendKeys("man@1234");
//		driver.findElement(REGISTERNECTARPOINTS).click();
//		driver.findElement(REGISTERNECTARPOINTS).sendKeys("12345678");
//		driver.findElement(COMPLETEREGISTER).click();
//	}
//
//	@Then("^User can see an assigned error message$")
//	public void user_can_see_an_assigned_error_message() throws Throwable {
//		Assert.assertEquals("Register with Tu", driver.findElement(By.cssSelector(".span-12.last h2")).getText());
//		System.out.println("please checkRecaptcha");
//	}
//
//	@When("^User tried to register with special characters and numbers$")
//	public void user_tried_to_register_with_special_characters_and_numbers() throws Throwable {
//		driver.findElement(LOGINREGISTER).click();
//		driver.findElement(REGISTERBUTTON).click(); // retoggle register
//		driver.findElement(REGISTEREMAIL).clear();
//		driver.findElement(REGISTEREMAIL).sendKeys("veena.gokula@gmail.com");
//		Select titleDropDown = new Select(driver.findElement(REGISTERTITLE));
//		titleDropDown.selectByIndex(3);
//		driver.findElement(REGISTERFIRSTNAME).clear();
//		driver.findElement(REGISTERFIRSTNAME).click();
//		driver.findElement(REGISTERFIRSTNAME).sendKeys("�$%%^&&^");
//		driver.findElement(REGISTERLASTNAME).clear();
//		driver.findElement(REGISTERLASTNAME).click();
//		driver.findElement(REGISTERLASTNAME).sendKeys("1234567");
//		driver.findElement(REGPASSWORD).clear();
//		driver.findElement(REGPASSWORD).click();
//		driver.findElement(REGPASSWORD).sendKeys("man@1234");
//		driver.findElement(REGCHECKPASSWORD).clear();
//		driver.findElement(REGCHECKPASSWORD).click();
//		driver.findElement(REGCHECKPASSWORD).sendKeys("man@1234");
//		driver.findElement(REGISTERNECTARPOINTS).click();
//		driver.findElement(REGISTERNECTARPOINTS).sendKeys("12345678");
//		driver.findElement(COMPLETEREGISTER).click();
//	}
//
//	@Then("^User will be seeing a error message$")
//	public void user_will_be_seeing_a_error_message() throws Throwable {
//		Assert.assertEquals("Register with Tu", driver.findElement(By.cssSelector(".span-12.last h2")).getText());
//		
//	}
//
//	@When("^password and confirm password are given different$")
//	public void password_and_confirm_password_are_given_different() throws Throwable {
//		driver.findElement(LOGINREGISTER).click();
//		driver.findElement(REGISTERBUTTON).click(); // retoggle register
//		driver.findElement(REGISTEREMAIL).clear();
//		driver.findElement(REGISTEREMAIL).sendKeys("veena.gokula@gmail.com");
//		Select titleDropDown = new Select(driver.findElement(REGISTERTITLE));
//		titleDropDown.selectByIndex(3);
//		driver.findElement(REGISTERFIRSTNAME).clear();
//		driver.findElement(REGISTERFIRSTNAME).click();
//		driver.findElement(REGISTERFIRSTNAME).sendKeys("veena");
//		driver.findElement(REGISTERLASTNAME).clear();
//		driver.findElement(REGISTERLASTNAME).click();
//		driver.findElement(REGISTERLASTNAME).sendKeys("gokula");
//		driver.findElement(REGPASSWORD).clear();
//		driver.findElement(REGPASSWORD).click();
//		driver.findElement(REGPASSWORD).sendKeys("manvik");
//		driver.findElement(REGCHECKPASSWORD).clear();
//		driver.findElement(REGCHECKPASSWORD).click();
//		driver.findElement(REGCHECKPASSWORD).sendKeys("manvik");
//		driver.findElement(REGISTERNECTARPOINTS).click();
//		driver.findElement(REGISTERNECTARPOINTS).sendKeys("12345678");
//		driver.findElement(COMPLETEREGISTER).click();
//	}
//
//	@Then("^error message popup password do not match$")
//	public void error_message_popup_password_do_not_match() throws Throwable {
//		Assert.assertEquals("Register with Tu", driver.findElement(By.cssSelector(".span-12.last h2")).getText());
//		
//	}
//
//	@When("^tried to register with minimum words$")
//	public void tried_to_register_with_minimum_words() throws Throwable {
//		driver.findElement(LOGINREGISTER).click();
//		driver.findElement(REGISTERBUTTON).click(); // retoggle register
//		driver.findElement(REGISTEREMAIL).clear();
//		driver.findElement(REGISTEREMAIL).sendKeys("veena.gokula@gmail.com");
//		Select titleDropDown = new Select(driver.findElement(REGISTERTITLE));
//		titleDropDown.selectByIndex(3);
//		driver.findElement(REGISTERFIRSTNAME).clear();
//		driver.findElement(REGISTERFIRSTNAME).click();
//		driver.findElement(REGISTERFIRSTNAME).sendKeys("veena");
//		driver.findElement(REGISTERLASTNAME).clear();
//		driver.findElement(REGISTERLASTNAME).click();
//		driver.findElement(REGISTERLASTNAME).sendKeys("gokula");
//		driver.findElement(REGPASSWORD).clear();
//		driver.findElement(REGPASSWORD).click();
//		driver.findElement(REGPASSWORD).sendKeys("man");
//		driver.findElement(REGCHECKPASSWORD).clear();
//		driver.findElement(REGCHECKPASSWORD).click();
//		driver.findElement(REGCHECKPASSWORD).sendKeys("man");
//		driver.findElement(REGISTERNECTARPOINTS).click();
//		driver.findElement(REGISTERNECTARPOINTS).sendKeys("12345678");
//		driver.findElement(COMPLETEREGISTER).click();
//	}
//
//	@Then("^error message pop up$")
//	public void error_message_pop_up() throws Throwable {
//		Assert.assertEquals("Register with Tu", driver.findElement(By.cssSelector(".span-12.last h2")).getText());
//		
//
//	}
//
//	@When("^User nectar card not mandatory$")
//	public void user_nectar_card_not_mandatory() throws Throwable {
//		driver.findElement(LOGINREGISTER).click();
//		driver.findElement(REGISTERBUTTON).click();
//		driver.findElement(REGISTEREMAIL).clear();
//		driver.findElement(REGISTEREMAIL).sendKeys("veena.gokula@gmail.com");
//		Select titleDropDown = new Select(driver.findElement(REGISTERTITLE));
//		titleDropDown.selectByIndex(3);
//		driver.findElement(REGISTERFIRSTNAME).clear();
//		driver.findElement(REGISTERFIRSTNAME).click();
//		driver.findElement(REGISTERFIRSTNAME).sendKeys("veena");
//		driver.findElement(REGISTERLASTNAME).clear();
//		driver.findElement(REGISTERLASTNAME).click();
//		driver.findElement(REGISTERLASTNAME).sendKeys("gokula");
//		driver.findElement(REGPASSWORD).clear();
//		driver.findElement(REGPASSWORD).click();
//		driver.findElement(REGPASSWORD).sendKeys("man@1234");
//		driver.findElement(REGCHECKPASSWORD).clear();
//		driver.findElement(REGCHECKPASSWORD).click();
//		driver.findElement(REGCHECKPASSWORD).sendKeys("man@1234");
//		driver.findElement(REGISTERNECTARPOINTS).click();
//		driver.findElement(REGISTERNECTARPOINTS).sendKeys("");
//		driver.findElement(COMPLETEREGISTER).click();
//	}
//
//	@Then("^User can register his email$")
//	public void user_can_register_his_email() throws Throwable {
//		Assert.assertEquals("Register with Tu", driver.findElement(By.cssSelector(".span-12.last h2")).getText());
//	}

//	@When("^terms and conditions are not accepted$")
//	public void terms_and_conditions_are_not_accepted() throws Throwable {
//		driver.findElement(LOGINREGISTER).click();
//		driver.findElement(REGISTERBUTTON).click(); // retoggle register
//		driver.findElement(REGISTEREMAIL).clear();
//		driver.findElement(REGISTEREMAIL).sendKeys("veena.gokula@gmail.com");
//		Select titleDropDown = new Select(driver.findElement(REGISTERTITLE));
//		titleDropDown.selectByIndex(3);
//		driver.findElement(REGISTERFIRSTNAME).clear();
//		driver.findElement(REGISTERFIRSTNAME).click();
//		driver.findElement(REGISTERFIRSTNAME).sendKeys("veena");
//		driver.findElement(REGISTERLASTNAME).clear();
//		driver.findElement(REGISTERLASTNAME).click();
//		driver.findElement(REGISTERLASTNAME).sendKeys("gokula");
//		driver.findElement(REGPASSWORD).clear();
//		driver.findElement(REGPASSWORD).click();
//		driver.findElement(REGPASSWORD).sendKeys("man@1234");
//		driver.findElement(REGCHECKPASSWORD).clear();
//		driver.findElement(REGCHECKPASSWORD).click();
//		driver.findElement(REGCHECKPASSWORD).sendKeys("man@1234");
//		driver.findElement(REGISTERNECTARPOINTS).click();
//		driver.findElement(REGISTERNECTARPOINTS).sendKeys("12345678");
//		driver.findElement(By.linkText("terms and conditions")).click();
//	}
//
//	@Then("^User cannot register$")
//	public void user_cannot_register() throws Throwable {
//		Assert.assertEquals("Register with Tu", driver.findElement(By.cssSelector(".span-12.last h2")).getText());
//		System.out.println("Please correct the errors below.");
//	}
//
//	@When("^image captcha button is not clicked$")
//	public void image_captcha_button_is_not_clicked() throws Throwable {
//		driver.findElement(LOGINREGISTER).click();
//		driver.findElement(REGISTERBUTTON).click();
//		driver.findElement(REGISTEREMAIL).clear();
//		driver.findElement(REGISTEREMAIL).sendKeys("veena.gokula@gmail.com");
//		Select titleDropDown = new Select(driver.findElement(REGISTERTITLE));
//		titleDropDown.selectByIndex(3);
//		driver.findElement(REGISTERFIRSTNAME).clear();
//		driver.findElement(REGISTERFIRSTNAME).click();
//		driver.findElement(REGISTERFIRSTNAME).sendKeys("veena");
//		driver.findElement(REGISTERLASTNAME).clear();
//		driver.findElement(REGISTERLASTNAME).click();
//		driver.findElement(REGISTERLASTNAME).sendKeys("gokula");
//		driver.findElement(REGPASSWORD).clear();
//		driver.findElement(REGPASSWORD).click();
//		driver.findElement(REGPASSWORD).sendKeys("man@1234");
//		driver.findElement(REGCHECKPASSWORD).clear();
//		driver.findElement(REGCHECKPASSWORD).click();
//		driver.findElement(REGCHECKPASSWORD).sendKeys("man@1234");
//		driver.findElement(REGISTERNECTARPOINTS).click();
//		driver.findElement(REGISTERNECTARPOINTS).sendKeys("12345678");
//		driver.findElement(COMPLETEREGISTER).click();
//	}
//
//	@Then("^User cannot register with the given details$")
//	public void user_cannot_register_with_the_given_details() throws Throwable {
//		Assert.assertEquals("Register with Tu", driver.findElement(By.cssSelector(".span-12.last h2")).getText());
//		Assert.assertEquals("Please check the recaptcha",
//				driver.findElement(By.cssSelector("#reCaptcha-register")).getText());
//
//	}
//
//	@When("^User tried to click on hide/show password$")
//	public void user_tried_to_click_on_hide_show_password() throws Throwable {
//		driver.findElement(LOGINREGISTER).click();
//		driver.findElement(REGISTERBUTTON).click(); // retoggle register
//		driver.findElement(REGISTEREMAIL).clear();
//		driver.findElement(REGISTEREMAIL).sendKeys(" ");
//		Select titleDropDown = new Select(driver.findElement(REGISTERTITLE));
//		titleDropDown.selectByIndex(3);
//		driver.findElement(REGISTERFIRSTNAME).clear();
//		driver.findElement(REGISTERFIRSTNAME).click();
//		driver.findElement(REGISTERFIRSTNAME).sendKeys("veena");
//		driver.findElement(REGISTERLASTNAME).clear();
//		driver.findElement(REGISTERLASTNAME).click();
//		driver.findElement(REGISTERLASTNAME).sendKeys("gokula");
//		driver.findElement(REGPASSWORD).clear();
//		driver.findElement(REGPASSWORD).click();
//		driver.findElement(REGPASSWORD).sendKeys("man@1234");
//		driver.findElement(SHOW).click();
//	}
//
//	@Then("^User should see the password which are in stars$")
//	public void user_should_see_the_password_which_are_in_stars() throws Throwable {
//		Assert.assertEquals("Register with Tu", driver.findElement(By.cssSelector(".span-12.last h2")).getText());
//	}
//
//	@When("^User given all mandatory required details$")
//	public void user_given_all_mandatory_required_details() throws Throwable {
//		driver.findElement(LOGINREGISTER).click();
//		driver.findElement(REGISTERBUTTON).click();
//		driver.findElement(REGISTEREMAIL).clear();
//		driver.findElement(REGISTEREMAIL).sendKeys("veena.gokula@gmail.com");
//		Select titleDropDown = new Select(driver.findElement(REGISTERTITLE));
//		titleDropDown.selectByIndex(3);
//		driver.findElement(REGISTERFIRSTNAME).clear();
//		driver.findElement(REGISTERFIRSTNAME).click();
//		driver.findElement(REGISTERFIRSTNAME).sendKeys("veena");
//		driver.findElement(REGISTERLASTNAME).clear();
//		driver.findElement(REGISTERLASTNAME).click();
//		driver.findElement(REGISTERLASTNAME).sendKeys("gokula");
//		driver.findElement(REGPASSWORD).clear();
//		driver.findElement(REGPASSWORD).click();
//		driver.findElement(REGPASSWORD).sendKeys("man@1234");
//		driver.findElement(REGCHECKPASSWORD).clear();
//		driver.findElement(REGCHECKPASSWORD).click();
//		driver.findElement(REGCHECKPASSWORD).sendKeys("man@1234");
//		driver.findElement(REGISTERNECTARPOINTS).click();
//		driver.findElement(REGISTERNECTARPOINTS).sendKeys("1234567");
//		driver.findElement(COMPLETEREGISTER).click();
//	}
//
//	@Then("^User can register with his email id and nectar card details$")
//	public void user_can_register_with_his_email_id_and_nectar_card_details() throws Throwable {
//		Assert.assertEquals("Register with Tu", driver.findElement(By.cssSelector(".span-12.last h2")).getText());
//		System.out.println("please check recaptcha");
//	}

	// Store Locator
//
//	@Given("^User on store locator page$")
//	public void user_on_store_locator_page() throws Throwable {
//		System.setProperty("webdriver.chrome.driver", "C:\\Selenium-API\\ChromeDriver\\chromedriver.exe");
//		driver = new ChromeDriver();
//		driver.get(baseURL);
//		driver.manage().window().maximize();
//		Assert.assertEquals("https://tuclothing.sainsburys.co.uk/", driver.getCurrentUrl());
//		driver.findElement(STORELOCATORlINK).click();
//		Thread.sleep(3000);
//		Assert.assertEquals("Store Locator", driver.findElement(By.cssSelector("h1")).getText());
//
//	}
//
//	@When("^User entered invalid postcode$")
//	public void user_entered_invalid_postcode() throws Throwable {
//		driver.findElement(POSTCODEBOX).click();
//		driver.findElement(POSTCODEBOX).clear();
//		driver.findElement(POSTCODEBOX).sendKeys("Tw0 045");
//		driver.findElement(FINDSTORES).click();
//	}
//
//	@Then("^error assigned sorry no result found$")
//	public void error_assigned_sorry_no_result_found() throws Throwable {
//		Assert.assertEquals(
//				"Sorry, no results found for your search. Please check your spelling and make sure you are searching for a town name or postcode.",
//				driver.findElement(By.cssSelector(".ln-u-display-inline-block.ln-u-soft")).getText());
//		System.out.println(
//				"Sorry, no results found for your search. Please check your spelling and make sure you are searching for a town name or postcode.");
//	}
//
//	@When("^User postcode field left blank$")
//	public void user_postcode_field_left_blank() throws Throwable {
//		driver.findElement(POSTCODEBOX).click();
//		driver.findElement(POSTCODEBOX).clear();
//		driver.findElement(POSTCODEBOX).sendKeys("   ");
//		driver.findElement(FINDSTORES).click();
//	}
//
//	@Then("^error assigned please fill the details$")
//	public void error_assigned_please_fill_the_details() throws Throwable {
//		System.out.println("Please fill Out deatils");
//		Assert.assertEquals("Store Locator", driver.findElement(By.cssSelector("h1")).getText());
//	}
//
//	@When("^given valid postcode and search location$")
//	public void given_valid_postcode_and_search_location() throws Throwable {
//		driver.findElement(POSTCODEBOX).click();
//		driver.findElement(POSTCODEBOX).clear();
//		driver.findElement(POSTCODEBOX).sendKeys("Tw3 1TA");
//		driver.findElement(FINDSTORES).click();
//		Thread.sleep(3000);
//	}
//	@Then("^click on toggle out button on map$")
//	public void click_on_toggle_out_button_on_map() throws Throwable {
//		Thread.sleep(3000);
//		driver.findElement(TOGGLEFULLSCREEN).click();
//	}
//
//	@Then("^system is allowed to toggleout$")
//	public void system_is_allowed_to_toggleout() throws Throwable {
//	Assert.assertEquals("Store Locator", driver.findElement(By.cssSelector("h1")).getText());
//	}
//
//
//	@When("^User click on map to satellite view$")
//	public void user_click_on_map_to_satellite_view() throws Throwable {
//		driver.findElement(POSTCODEBOX).click();
//		driver.findElement(POSTCODEBOX).clear();
//		driver.findElement(POSTCODEBOX).sendKeys("Tw3 1TA");
//		driver.findElement(FINDSTORES).click();
//		Thread.sleep(3000);
//		driver.findElement(SATELLITE).click();
//	}
//
//	@Then("^system should allow you to switch from maps to satellite$")
//	public void system_should_allow_you_to_switch_from_maps_to_satellite() throws Throwable {
//		Thread.sleep(4000);
//		driver.findElement(MAP).click();
//		Assert.assertEquals("Store Locator", driver.findElement(By.cssSelector("h1")).getText());
//	}
//
//	@When("^given valid postcode$")
//	public void given_valid_postcode() throws Throwable {
//		driver.findElement(POSTCODEBOX).click();
//		driver.findElement(POSTCODEBOX).clear();
//		driver.findElement(POSTCODEBOX).sendKeys("Tw3 1TA");
//	}
//
//	@When("^click childrens clothing option$")
//	public void click_childrens_clothing_option() throws Throwable {
//		Thread.sleep(3000);
//		driver.findElement(CHILDREN).click();
//		Thread.sleep(3000);
//	}
//
//	@Then("^search display childrens clothing stores for click and collect$")
//	public void search_display_childrens_clothing_stores_for_click_and_collect() throws Throwable {
//		driver.findElement(CLICKANDCOLLECT).click();
//		Thread.sleep(3000);
//		driver.findElement(FINDSTORES).click();
//		Assert.assertEquals("Your nearest stores", driver.findElement(By.cssSelector("#header1")).getText());
//
//	}
//
//	@When("^click women clothing option$")
//	public void click_women_clothing_option() throws Throwable {
//		driver.findElement(STORELOCATORlINK).click();
//		Assert.assertEquals("Store Locator", driver.findElement(By.cssSelector("h1")).getText());
//		driver.findElement(POSTCODEBOX).click();
//		driver.findElement(POSTCODEBOX).clear();
//		driver.findElement(POSTCODEBOX).sendKeys("Tw3 1TA");
//		Thread.sleep(3000);
//		driver.findElement(WOMENS).click();
//	}
//
//	@Then("^search display women clothing stores for click and collect$")
//	public void search_display_women_clothing_stores_for_click_and_collect() throws Throwable {
//		driver.findElement(CLICKANDCOLLECT).click();
//		driver.findElement(FINDSTORES).click();
//		Assert.assertEquals("Your nearest stores", driver.findElement(By.cssSelector("#header1")).getText());
//	}
//
//	@When("^click men clothing option$")
//	public void click_men_clothing_option() throws Throwable {
//		driver.findElement(POSTCODEBOX).click();
//		driver.findElement(POSTCODEBOX).clear();
//		driver.findElement(POSTCODEBOX).sendKeys("Tw3 1TA");
//		Thread.sleep(3000);
//		driver.findElement(MENS).click();
//	}
//
//	@Then("^search display men clothing stores for click and collect$")
//	public void search_display_men_clothing_stores_for_click_and_collect() throws Throwable {
//		driver.findElement(FINDSTORES).click();
//		Assert.assertEquals("Your nearest stores", driver.findElement(By.cssSelector("#header1")).getText());
//	}
//
//	@When("^User enter postcode on respective field$")
//	public void user_enter_postcode_on_respective_field() throws Throwable {
//		driver.findElement(POSTCODEBOX).click();
//		driver.findElement(POSTCODEBOX).clear();
//		driver.findElement(POSTCODEBOX).sendKeys("TW31TA");
//	}
//
//	@Then("^stores nearby are displayed$")
//	public void stores_nearby_are_displayed() throws Throwable {
//		driver.findElement(FINDSTORES).click();
//		Assert.assertEquals("Your nearest stores", driver.findElement(By.cssSelector("#header1")).getText());
//	}
//
//	
//	
//	
//	@When("^User click store locator link$")
//	public void user_click_store_locator_link() throws Throwable {
//		driver.findElement(STORELOCATORlINK).click();
//	   
//	}
//
//
//
//	@Then("^store locator page will be displayed$")
//	public void store_locator_page_will_be_displayed() throws Throwable {
//		Assert.assertEquals("Store Locator", driver.findElement(By.cssSelector("h1")).getText());
//	}
//
//	@When("^provide valid postcode$")
//	public void provide_valid_postcode() throws Throwable {
//		driver.findElement(POSTCODEBOX).click();
//		driver.findElement(POSTCODEBOX).clear();
//		driver.findElement(POSTCODEBOX).sendKeys("Tw3 1TA");
//	}
//
//	@When("^search is done to check nearest click and collect stores$")
//	public void search_is_done_to_check_nearest_click_and_collect_stores() throws Throwable {
//		driver.findElement(CLICKANDCOLLECT).click();
//	}
//
//	@Then("^system displayed all near by click and collect stores$")
//	public void system_displayed_all_near_by_click_and_collect_stores() throws Throwable {
//		driver.findElement(FINDSTORES).click();
//		Assert.assertEquals("Your nearest stores", driver.findElement(By.cssSelector("#header1")).getText());
//	}
////
//	@When("^click children check box with valid postcode$")
//	public void click_children_check_box_with_valid_postcode() throws Throwable {
//		driver.findElement(POSTCODEBOX).click();
//		driver.findElement(POSTCODEBOX).clear();
//		driver.findElement(POSTCODEBOX).sendKeys("Tw3 1TA");
//	}
//
//	@When("^search with find stores$")
//	public void search_with_find_stores() throws Throwable {
//		driver.findElement(CHILDREN).click();
//	}
//
//	@Then("^search will display children clothing stores$")
//	public void search_will_display_children_clothing_stores() throws Throwable {
//		driver.findElement(FINDSTORES).click();
//		Assert.assertEquals("Your nearest stores", driver.findElement(By.cssSelector("#header1")).getText());
//	}
//
//	@When("^select men clothing with a valid postcode$")
//	public void select_men_clothing_with_a_valid_postcode() throws Throwable {
//		driver.findElement(POSTCODEBOX).click();
//		driver.findElement(POSTCODEBOX).clear();
//		driver.findElement(POSTCODEBOX).sendKeys("Tw3 1TA");
//		driver.findElement(MENS).click();
//	}
//
//	@Then("^search display men clothing stores$")
//	public void search_display_men_clothing_stores() throws Throwable {
//		driver.findElement(FINDSTORES).click();
//		Assert.assertEquals("Your nearest stores", driver.findElement(By.cssSelector("#header1")).getText());
//	}
//
//	@When("^select women clothing with a valid postcode$")
//	public void select_women_clothing_with_a_valid_postcode() throws Throwable {
//		driver.findElement(POSTCODEBOX).click();
//		driver.findElement(POSTCODEBOX).clear();
//		driver.findElement(POSTCODEBOX).sendKeys("Tw3 1TA");
//		driver.findElement(WOMENS).click();
//	}
//
//	@Then("^search display women clothing stores$")
//	public void search_display_women_clothing_stores() throws Throwable {
//		driver.findElement(FINDSTORES).click();
//		Assert.assertEquals("Your nearest stores", driver.findElement(By.cssSelector("#header1")).getText());
//	}
//
//	@When("^search is done to check the nearest stores$")
//	public void search_is_done_to_check_the_nearest_stores() throws Throwable {
//		driver.findElement(POSTCODEBOX).click();
//		driver.findElement(POSTCODEBOX).clear();
//		driver.findElement(POSTCODEBOX).sendKeys("Tw3 1TA");
//	}
//
//	@Then("^User will be able to zoom in the exact store address on map$")
//	public void user_will_be_able_to_zoom_in_the_exact_store_address_on_map() throws Throwable {
//		driver.findElement(FINDSTORES).click();
//		Thread.sleep(3000);
//		driver.findElement(ZOOMIN).click();
//		Assert.assertEquals("Store Locator", driver.findElement(By.cssSelector("h1")).getText());
//	}
//
//	@When("^search is done to check with the nearest stores$")
//	public void search_is_done_to_check_with_the_nearest_stores() throws Throwable {
//		driver.findElement(POSTCODEBOX).click();
//		driver.findElement(POSTCODEBOX).clear();
//		driver.findElement(POSTCODEBOX).sendKeys("Tw3 1TA");
//	}
//		@Then("^User will be able to zoom out  on map$")
//		public void user_will_be_able_to_zoom_out_on_map() throws Throwable {
//		driver.findElement(FINDSTORES).click();
//		Thread.sleep(3000);
//		driver.findElement(ZOOMOUT).click();
//		Assert.assertEquals("Store Locator", driver.findElement(By.cssSelector("h1")).getText());
//	}

// Add product to Basket
//
//
//@Given("^User on product details page$")
//public void user_on_product_details_page() throws Throwable {
//	System.setProperty("webdriver.chrome.driver", "C:\\Selenium-API\\ChromeDriver\\chromedriver.exe");
//	driver = new ChromeDriver();
//	driver.get(baseURL);
//	driver.manage().window().maximize();
//	Assert.assertEquals("https://tuclothing.sainsburys.co.uk/", driver.getCurrentUrl());
//	//driver.findElement(LOGINREGISTER).click();
//}
////
//@When("^User entered valid data$")  // recaptcha prob cannot login
//public void user_entered_valid_data() throws Throwable {
//	Thread.sleep(3000);
//	driver.findElement(EMAILID).clear();
//	driver.findElement(EMAILID).click();
//	driver.findElement(EMAILID).sendKeys("veena.gokula@gmail.com");
//	driver.findElement(PASSWORD).clear();
//	driver.findElement(PASSWORD).click();
//	driver.findElement(PASSWORD).sendKeys("man@1234");
//	driver.findElement(lOGINBUTTON).click();
//}
//
//@When("^select product and add to basket$")   // 
//public void select_product_and_add_to_basket() throws Throwable {
//	driver.findElement(WOMENLINK).click();
//	 Assert.assertEquals("Womens",driver.findElement(By.cssSelector(".ln-u-border-bottom--double")).getText());
//	Thread.sleep(3000);
//	driver.findElement(NEWARRIVALS).click();
//	 Assert.assertEquals("Womens Clothing New In", driver.findElement(By.cssSelector("h1")).getText());
//	Thread.sleep(3000);
//	driver.findElements(ITEMIMAGESELECTOR).get(3).click(); // First product of the page
//	Thread.sleep(3000);
//	driver.findElements(SIZECHECKBOX).get(1).click();
//	Thread.sleep(3000);
//	Select quantityDropDown = new Select(driver.findElement(By.cssSelector("#productVariantQty")));
//	quantityDropDown.selectByIndex(1);
//	driver.findElement(ADDTOCART).click();
//	Thread.sleep(3000);
//	
//}
//
//@Then("^User  allowed to add item to basket$")
//public void user_allowed_to_add_item_to_basket() throws Throwable {
//	driver.findElement(MINIBASKET).click(); // minibasket
//	Thread.sleep(3000);
//	Assert.assertEquals("Qty:2", driver.findElement(By.cssSelector(".prod_quantity")).getText());
//}
//
//@When("^User selected a product and add to basket$")
//public void user_selected_a_product_and_add_to_basket() throws Throwable {
//	Thread.sleep(3000);
//	driver.findElement(WOMENLINK).click();
//	Thread.sleep(3000);
//	driver.findElement(NEWARRIVALS).click();
//	Thread.sleep(3000);
//	driver.findElements(ITEMIMAGESELECTOR).get(3).click();
//	Thread.sleep(3000);
//	driver.findElements(SIZECHECKBOX).get(1).click();
//	Thread.sleep(4000);
//	Select quantityDropDown = new Select(driver.findElement(By.cssSelector("#productVariantQty")));
//	quantityDropDown.selectByIndex(1);
//	driver.findElement(ADDTOCART).click();
//	
//}
//
//@Then("^User is allowed without login to add items to basket$")
//public void user_is_allowed_without_login_to_add_items_to_basket() throws Throwable {
//	Thread.sleep(3000);
//	driver.findElement(MINIBASKET).click(); // minibasket
//	Thread.sleep(3000);
//	Assert.assertEquals("Qty:2", driver.findElement(By.cssSelector(".prod_quantity")).getText());
//}
//
//@When("^User selected multiple products and add to basket$")
//public void user_selected_multiple_products_and_add_to_basket() throws Throwable {
//	driver.findElement(WOMENLINK).click();
//	Thread.sleep(3000);
//	driver.findElement(NEWARRIVALS).click();
//	Thread.sleep(3000);
//	driver.findElements(ITEMIMAGESELECTOR).get(3).click();
//	Thread.sleep(3000);
//	driver.findElements(SIZECHECKBOX).get(1).click();
//	Thread.sleep(4000);
//	Select quantityDropDown = new Select(driver.findElement(By.cssSelector("#productVariantQty")));
//	quantityDropDown.selectByIndex(1);
//	driver.findElement(ADDTOCART).click();
//	Thread.sleep(3000);
//	driver.findElement(MINIBASKET).click(); // minibasket
//	Thread.sleep(3000);
//	driver.findElement(MINIVIEWBASKET).click(); // minibasket view basket
//	
//	Assert.assertEquals("Qty:2", driver.findElement(By.cssSelector(".prod_quantity")).getText());
//	driver.findElement(WOMENLINK).click();
//	Thread.sleep(3000);
//	driver.findElement(NEWARRIVALS).click();
//	Thread.sleep(3000);
//	driver.findElements(ITEMIMAGESELECTOR).get(4).click();
//	Thread.sleep(3000);
//	driver.findElements(SIZECHECKBOX).get(1).click();
//	Thread.sleep(4000);
//	Select quantityDropDown1 = new Select(driver.findElement(By.cssSelector("#productVariantQty")));
//	quantityDropDown1.selectByIndex(2);
//	driver.findElement(ADDTOCART).click();
//	Thread.sleep(3000);
//	
//}
//
//@Then("^User is allowed without login to add multiple items to basket$")
//public void user_is_allowed_without_login_to_add_multiple_items_to_basket() throws Throwable {
//	driver.findElement(MINIBASKET).click(); // minibasket
//	Thread.sleep(3000);
//	driver.findElement(MINIVIEWBASKET).click(); // minibasket view basket
//	Assert.assertEquals("Qty:3", driver.findElement(By.cssSelector(".prod_quantity")).getText());
//	
//}
//@When("^User login and select multiple products and add to basket$")  // recaptcha prob so did not write code for login
//public void user_login_and_select_multiple_products_and_add_to_basket() throws Throwable {
//	driver.findElement(WOMENLINK).click();
//	Thread.sleep(3000);
//	driver.findElement(NEWARRIVALS).click();
//	Thread.sleep(3000);
//	driver.findElements(ITEMIMAGESELECTOR).get(3).click();
//	Thread.sleep(3000);
//	driver.findElements(SIZECHECKBOX).get(1).click();
//	Thread.sleep(4000);
//	Select quantityDropDown = new Select(driver.findElement(By.cssSelector("#productVariantQty")));
//	quantityDropDown.selectByIndex(1);
//	driver.findElement(ADDTOCART).click();
//	Thread.sleep(3000);
//	driver.findElement(MINIBASKET).click(); // minibasket
//	Thread.sleep(3000);
//	driver.findElement(MINIVIEWBASKET).click(); // minibasket view basket
//	
//	Assert.assertEquals("Qty:2", driver.findElement(By.cssSelector(".prod_quantity")).getText());
//	driver.findElement(WOMENLINK).click();
//	Thread.sleep(3000);
//	driver.findElement(NEWARRIVALS).click();
//	Thread.sleep(3000);
//	driver.findElements(ITEMIMAGESELECTOR).get(4).click();
//	Thread.sleep(3000);
//	driver.findElements(SIZECHECKBOX).get(1).click();
//	Thread.sleep(4000);
//	Select quantityDropDown1 = new Select(driver.findElement(By.cssSelector("#productVariantQty")));
//	quantityDropDown1.selectByIndex(1);
//	driver.findElement(ADDTOCART).click();
//	Thread.sleep(3000);
//	
//}
//
//@Then("^User allowed to add products to basket$")
//public void user_allowed_to_add_products_to_basket() throws Throwable {
//	driver.findElement(MINIBASKET).click(); // minibasket
//	Thread.sleep(3000);
//	driver.findElement(MINIVIEWBASKET).click(); // minibasket view basket
//	Assert.assertEquals("Qty:2", driver.findElement(By.cssSelector(".prod_quantity")).getText());
//
//}
//@When("^User select to remove products from basket$")
//public void user_select_to_remove_products_from_basket() throws Throwable {
//	driver.findElement(WOMENLINK).click();
//	Thread.sleep(3000);
//	driver.findElement(NEWARRIVALS).click();
//	Thread.sleep(3000);
//	driver.findElements(ITEMIMAGESELECTOR).get(3).click();
//	Thread.sleep(3000);
//	driver.findElements(SIZECHECKBOX).get(1).click();
//	Thread.sleep(4000);
//	Select quantityDropDown = new Select(driver.findElement(By.cssSelector("#productVariantQty")));
//	quantityDropDown.selectByIndex(2);
//	driver.findElement(ADDTOCART).click();
//	Thread.sleep(3000);
//	
//}
//
//@Then("^product should be removed from the basket$")
//public void product_should_be_removed_from_the_basket() throws Throwable {
//	driver.findElement(VIEWBASKET).click();
//	Thread.sleep(3000);
//	driver.findElement(CLICKVIEWBASKET).click(); // clickview basket
//	Thread.sleep(6000);
//	driver.findElement(REMOVEPRODUCT).click(); // remove product
//	Thread.sleep(3000);
//	driver.findElement(BASKETEMPTY).click();
//	
//	Assert.assertEquals("No items in your basket", driver.findElement(By.cssSelector(".cart_icon")).getText());
//	
//}
//
//@When("^there is no availability in size$")
//public void there_is_no_availability_in_size() throws Throwable {
//
//	driver.findElement(WOMENLINK).click();
//	Thread.sleep(3000);
//	driver.findElement(NEWARRIVALS).click();
//	Thread.sleep(3000);
//	driver.findElements(ITEMIMAGESELECTOR).get(3).click();		
//	Thread.sleep(3000);
//	
//	
//}
//
//@Then("^size icon button should be disabled$")
//	public void size_icon_button_should_be_disabled() throws Throwable {
//		//driver.findElement(SIZEDISABLED).click();
//	//driver.findElements(By.cssSelector(".tile.ln-u-border.ln-u-push-bottom.disabled ")).get(3);
//	//Assert.assertEquals("Product Details", driver.findElement(By.cssSelector("#tu-product-details")).getText());	
//}
//
//@When("^User added a product to the basket$")   // due to low stock user cannot order 5 quantities 
//public void user_added_a_product_to_the_basket() throws Throwable {
//	driver.findElement(WOMENLINK).click();
//	Thread.sleep(3000);
//	driver.findElement(NEWARRIVALS).click();
//	Thread.sleep(3000);
//	driver.findElements(ITEMIMAGESELECTOR).get(3).click();
//	Thread.sleep(3000);
//	driver.findElements(SIZECHECKBOX).get(1).click();
//	Thread.sleep(4000);
//	Select quantityDropDown = new Select(driver.findElement(By.cssSelector("#productVariantQty")));
//	quantityDropDown.selectByIndex(4);
//	driver.findElement(ADDTOCART).click();
//	
//}
//
//@Then("^quantity should not be greater than the stock$")
//public void quantity_should_not_be_greater_than_the_stock() throws Throwable {
//	System.out.println("Due to low stock number of items cannot be added to basket");
//	
//}

// check out basket
//
//@Given("^User on checkout page$")
//public void user_on_checkout_page() throws Throwable {
//	System.setProperty("webdriver.chrome.driver", "C:\\Selenium-API\\ChromeDriver\\chromedriver.exe");
//	driver = new ChromeDriver();
//	driver.get(baseURL);
//	driver.manage().window().maximize();
//	Assert.assertEquals("https://tuclothing.sainsburys.co.uk/", driver.getCurrentUrl());
//}
//
//@When("^User  increase or decrease the quantity$")
//public void user_increase_or_decrease_the_quantity() throws Throwable {
//	driver.findElement(WOMENLINK).click();
//	Thread.sleep(3000);
//	driver.findElement(NEWARRIVALS).click();
//	Thread.sleep(3000);
//	driver.findElements(ITEMIMAGESELECTOR).get(3).click();
//	Thread.sleep(3000);
//	driver.findElements(SIZECHECKBOX).get(1).click();
//	Thread.sleep(4000);
//	Select quantityDropDown = new Select(driver.findElement(By.cssSelector("#productVariantQty")));
//	quantityDropDown.selectByIndex(1);
//	driver.findElement(ADDTOCART).click();
//	Thread.sleep(5000);
//	driver.findElement(VIEWBASKET).click();
//	Thread.sleep(4000);
//	driver.findElement(CLICKVIEWBASKET).click();    //view basket and checkout
//	//update quantity
//	Thread.sleep(5000);
//	Select quantity1DropDown = new Select(driver.findElement(By.cssSelector("#quantity0")));
//	quantity1DropDown.selectByIndex(2);
//	Thread.sleep(2000);
//	driver.findElement(QUANTITYUPDATE).click(); //increasing quantity
//	//decrease quantity		
//	Thread.sleep(5000);
//	Select quantity2DropDown = new Select(driver.findElement(By.cssSelector("#quantity0")));
//	quantity2DropDown.selectByVisibleText("1");
//}
//
//@Then("^User allowed to click and update the items$")
//public void user_allowed_to_click_and_update_the_items() throws Throwable {
//	driver.findElement(QUANTITYUPDATE).click();
//	Assert.assertEquals("Product quantity has been updated.", driver.findElement(By.cssSelector("#globalMessages")).getText());
//}

		/*
		 * @Given("^I am in basket page$") public void i_am_in_basket_page() throws
		 * Throwable { System.setProperty("webdriver.chrome.driver",
		 * "C:\\Selenium-API\\ChromeDriver\\chromedriver.exe"); driver = new
		 * ChromeDriver(); driver.get(baseURL); driver.manage().window().maximize();
		 * Assert.assertEquals("https://tuclothing.sainsburys.co.uk/",
		 * driver.getCurrentUrl());
		 * 
		 * }
		 */

		/*
		 * @When("^I am in checkout basket page$") public void
		 * i_am_in_checkout_basket_page() throws Throwable {
		 * driver.findElement(WOMENLINK).click(); Thread.sleep(3000);
		 * driver.findElement(NEWARRIVALS).click(); Thread.sleep(3000);
		 * driver.findElements(ITEMIMAGESELECTOR).get(3).click(); Thread.sleep(3000);
		 * driver.findElements(SIZECHECKBOX).get(1).click(); Thread.sleep(4000); Select
		 * quantityDropDown = new
		 * Select(driver.findElement(By.cssSelector("#productVariantQty")));
		 * quantityDropDown.selectByIndex(1); driver.findElement(ADDTOCART).click();
		 * driver.findElement(WOMENLINK).click(); Thread.sleep(3000);
		 * driver.findElement(NEWARRIVALS).click(); Thread.sleep(3000);
		 * driver.findElements(ITEMIMAGESELECTOR).get(4).click(); Thread.sleep(4000);
		 * driver.findElements(SIZECHECKBOX).get(1).click(); Thread.sleep(4000); Select
		 * quantityDropDown1 = new
		 * Select(driver.findElement(By.cssSelector("#productVariantQty")));
		 * quantityDropDown1.selectByIndex(1); driver.findElement(ADDTOCART).click();
		 * driver.findElement(CHECKOUTBUTTON).click(); Thread.sleep(3000); }
		 */

		/*
		 * @Then("^the items can be removed from check out box$") public void
		 * the_items_can_be_removed_from_check_out_box() throws Throwable {
		 * driver.findElement(REMOVEPRODUCT).click();
		 * driver.findElement(REMOVEPRODUCT).click(); // 2 products removed
		 * Assert.assertEquals("Product has been removed from your cart.",
		 * driver.findElement(By.cssSelector("#globalMessages")).getText()); }
		 */

//@When("^invalid  voucher added  to the checkout basket$")
//public void invalid_voucher_added_to_the_checkout_basket() throws Throwable {
//	driver.findElement(WOMENLINK).click();
//	Thread.sleep(3000);
//	driver.findElement(NEWARRIVALS).click();
//	Thread.sleep(3000);
//	driver.findElements(ITEMIMAGESELECTOR).get(3).click();
//	Thread.sleep(3000);
//	driver.findElements(SIZECHECKBOX).get(1).click();
//	Thread.sleep(4000);
//	Select quantityDropDown = new Select(driver.findElement(By.cssSelector("#productVariantQty")));
//	quantityDropDown.selectByIndex(1);
//	driver.findElement(ADDTOCART).click();	
//	Thread.sleep(4000);
//	driver.findElement(CHECKOUTBUTTON).click();
//	Thread.sleep(5000);
//	driver.findElement(SHOWPROMO).click();
//	Thread.sleep(3000);
//	driver.findElement(VOUCHERPAY).click();
//	Thread.sleep(3000);
//	driver.findElement(VOUCHERPAY).sendKeys("0000000");
//	
//}

		/*
		 * @Then("^the discount is not applicable$") public void
		 * the_discount_is_not_applicable() throws Throwable {
		 * Assert.assertEquals("https://tuclothing.sainsburys.co.uk/cart",
		 * driver.getCurrentUrl()); Thread.sleep(3000);
		 * driver.findElement(VOUCHERAPPLY).click();
		 * 
		 * }
		 */
@When("^I added valid voucher to the check out basket$")
public void i_added_valid_voucher_to_the_check_out_basket() throws Throwable {
	driver.findElement(WOMENLINK).click();
	Thread.sleep(3000);
	driver.findElement(NEWARRIVALS).click();
	Thread.sleep(3000);
	driver.findElements(ITEMIMAGESELECTOR).get(3).click();
	Thread.sleep(3000);
	driver.findElements(SIZECHECKBOX).get(1).click();
	Thread.sleep(4000);
	Select quantityDropDown = new Select(driver.findElement(By.cssSelector("#productVariantQty")));
	quantityDropDown.selectByIndex(1);
	driver.findElement(ADDTOCART).click();
	Thread.sleep(4000);
	driver.findElement(CHECKOUTBUTTON).click();
	Thread.sleep(3000);
	driver.findElement(SHOWPROMO).click();
	Thread.sleep(3000);
}

@Then("^discount should be accepted$")
public void discount_should_be_accepted() throws Throwable {
	driver.findElement(VOUCHERPAY).click();
	Thread.sleep(3000);
	driver.findElement(VOUCHERPAY).sendKeys("0000000");
	Thread.sleep(3000);
	Assert.assertEquals("https://tuclothing.sainsburys.co.uk/cart", driver.getCurrentUrl());
	driver.findElement(VOUCHERAPPLY).click(); // cannot be clicked as no voucher code
}
/*
 * @When("^User can decrease the number of items in basket page$") public void
 * user_can_decrease_the_number_of_items_in_basket_page() throws Throwable {
 * driver.findElement(WOMENLINK).click(); Thread.sleep(3000);
 * driver.findElement(NEWARRIVALS).click(); Thread.sleep(3000);
 * driver.findElements(ITEMIMAGESELECTOR).get(0).click(); Thread.sleep(3000);
 * driver.findElements(SIZECHECKBOX).get(1).click(); Thread.sleep(4000); Select
 * quantityDropDown = new
 * Select(driver.findElement(By.cssSelector("#productVariantQty")));
 * quantityDropDown.selectByIndex(1); driver.findElement(ADDTOCART).click();
 * Thread.sleep(5000); driver.findElement(VIEWBASKET).click();
 * Thread.sleep(4000); driver.findElement(CLICKVIEWBASKET).click(); //view
 * basket and checkout //decrease quantity Thread.sleep(5000); Select
 * quantity2DropDown = new
 * Select(driver.findElement(By.cssSelector("#quantity0")));
 * quantity2DropDown.selectByVisibleText("1"); Thread.sleep(3000); }
 */

/*
 * @Then("^User quantity is updated in checkout page$") public void
 * user_quantity_is_updated_in_checkout_page() throws Throwable {
 * driver.findElement(QUANTITYUPDATE).click();
 * Assert.assertEquals("Product quantity has been updated.",
 * driver.findElement(By.cssSelector("#globalMessages")).getText()); }
 */
/*
 * @When("^User on basket page can increase the number of items$") public void
 * user_on_basket_page_can_increase_the_number_of_items() throws Throwable {
 * driver.findElement(WOMENLINK).click(); Thread.sleep(3000);
 * driver.findElement(NEWARRIVALS).click(); Thread.sleep(3000);
 * driver.findElements(ITEMIMAGESELECTOR).get(0).click(); Thread.sleep(3000);
 * driver.findElements(SIZECHECKBOX).get(1).click(); Thread.sleep(4000); Select
 * quantityDropDown = new
 * Select(driver.findElement(By.cssSelector("#productVariantQty")));
 * quantityDropDown.selectByIndex(1); driver.findElement(ADDTOCART).click();
 * Thread.sleep(5000); driver.findElement(VIEWBASKET).click();
 * Thread.sleep(4000); driver.findElement(CLICKVIEWBASKET).click(); //view
 * basket and checkout //String s=driver.findElement(By.
 * cssSelector("a[class='ln-c-button ln-c-button--primary']")).getText();
 * //System.out.println("valu::"+s); //update quantity Thread.sleep(5000);
 * Select quantity1DropDown = new
 * Select(driver.findElement(By.cssSelector("#quantity0")));
 * quantity1DropDown.selectByIndex(2); Thread.sleep(2000); }
 * 
 * @Then("^User increased quantity is updated in check out page$") public void
 * user_increased_quantity_is_updated_in_check_out_page() throws Throwable {
 * driver.findElement(QUANTITYUPDATE).click(); //increasing quantity
 * Assert.assertEquals("Product quantity has been updated.",
 * driver.findElement(By.cssSelector("#globalMessages")).getText()); }
 */

/*
 * @When("^User can add maximum number of products$") public void
 * user_can_add_maximum_number_of_products() throws Throwable {
 * driver.findElement(WOMENLINK).click(); //prod 1 Thread.sleep(3000);
 * driver.findElement(NEWARRIVALS).click(); Thread.sleep(3000);
 * driver.findElements(ITEMIMAGESELECTOR).get(4).click(); Thread.sleep(3000);
 * driver.findElements(SIZECHECKBOX).get(1).click(); Thread.sleep(4000); Select
 * quantityDropDown = new
 * Select(driver.findElement(By.cssSelector("#productVariantQty")));
 * quantityDropDown.selectByIndex(1); driver.findElement(ADDTOCART).click();
 * Thread.sleep(4000); driver.findElement(WOMENLINK).click(); //prod 2
 * Thread.sleep(3000); driver.findElement(NEWARRIVALS).click();
 * Thread.sleep(3000); driver.findElements(ITEMIMAGESELECTOR).get(3).click();
 * Thread.sleep(3000); driver.findElements(SIZECHECKBOX).get(1).click();
 * Thread.sleep(4000); Select quantityDropDown1 = new
 * Select(driver.findElement(By.cssSelector("#productVariantQty")));
 * quantityDropDown1.selectByIndex(1); driver.findElement(ADDTOCART).click(); //
 * product 3 Thread.sleep(4000); driver.findElement(WOMENLINK).click();
 * Thread.sleep(3000); driver.findElement(NEWARRIVALS).click();
 * Thread.sleep(3000); driver.findElements(ITEMIMAGESELECTOR).get(6).click();
 * Thread.sleep(3000); driver.findElements(SIZECHECKBOX).get(1).click();
 * Thread.sleep(4000); Select quantityDropDown2 = new
 * Select(driver.findElement(By.cssSelector("#productVariantQty")));
 * quantityDropDown2.selectByIndex(1); driver.findElement(ADDTOCART).click(); }
 */

/*
 * @Then("^maximum products are added to checkout basket$") public void
 * maximum_products_are_added_to_checkout_basket() throws Throwable {
 * driver.findElement(VIEWBASKET).click(); Thread.sleep(4000);
 * driver.findElement(CLICKVIEWBASKET).click(); //view basket and checkout
 * Assert.assertEquals("6�items",
 * driver.findElement(By.cssSelector(".cartItemsText")).getText()); }
 */

/*
 * @When("^User added products to basket and checkout$") public void
 * user_added_products_to_basket_and_checkout() throws Throwable {
 * driver.findElement(WOMENLINK).click(); //prod 1 Thread.sleep(3000);
 * driver.findElement(NEWARRIVALS).click(); Thread.sleep(3000);
 * driver.findElements(ITEMIMAGESELECTOR).get(0).click(); Thread.sleep(3000);
 * driver.findElements(SIZECHECKBOX).get(1).click(); Thread.sleep(4000); Select
 * quantityDropDown = new
 * Select(driver.findElement(By.cssSelector("#productVariantQty")));
 * quantityDropDown.selectByIndex(1);
 * 
 * driver.findElement(ADDTOCART).click(); Thread.sleep(6000);
 * driver.findElement(CHECKOUTBUTTON).click(); //check out button }
 */

/*
 * @Then("^the checkout page will navigate user to delivery option page$")
 * public void the_checkout_page_will_navigate_user_to_delivery_option_page()
 * throws Throwable { Thread.sleep(4000);
 * driver.findElement(PROCEEDTOCHECKOUTUP).click(); // proceed to check out
 * button
 * Assert.assertEquals("https://tuclothing.sainsburys.co.uk/login/checkout",
 * driver.getCurrentUrl()); }
 */
/*
 * @When("^User added products to basket$") public void
 * user_added_products_to_basket() throws Throwable {
 * driver.findElement(WOMENLINK).click(); //prod 1 Thread.sleep(3000);
 * driver.findElement(NEWARRIVALS).click(); Thread.sleep(3000);
 * driver.findElements(ITEMIMAGESELECTOR).get(4).click(); Thread.sleep(3000);
 * driver.findElements(SIZECHECKBOX).get(1).click(); Thread.sleep(4000); Select
 * quantityDropDown = new
 * Select(driver.findElement(By.cssSelector("#productVariantQty")));
 * quantityDropDown.selectByIndex(1);
 * 
 * driver.findElement(ADDTOCART).click(); }
 */
/*
 * @Then("^mini basket cart  is allowed to click and view the items$") public
 * void mini_basket_cart_is_allowed_to_click_and_view_the_items() throws
 * Throwable { Thread.sleep(6000); driver.findElement(MINIBASKET).click(); //
 * minibasket Assert.assertEquals(
 * "https://tuclothing.sainsburys.co.uk/p/Spot-Print-Tiered-Top/137093914-Mono?searchTerm=:newArrivals&searchProduct=",
 * driver.getCurrentUrl()); }
 */

//Click and Collect


/*
 * @Given("^I am in checkout page as a guest in click and collect$") public void
 * i_am_in_checkout_page_as_a_guest_in_click_and_collect() throws Throwable {
 * System.setProperty("webdriver.chrome.driver",
 * "C:\\Selenium-API\\ChromeDriver\\chromedriver.exe"); driver = new
 * ChromeDriver(); driver.get(baseURL); driver.manage().window().maximize();
 * Assert.assertEquals("https://tuclothing.sainsburys.co.uk/",
 * driver.getCurrentUrl());
 * 
 * driver.findElement(WOMENLINK).click(); Thread.sleep(3000);
 * driver.findElement(NEWARRIVALS).click(); Thread.sleep(3000);
 * driver.findElements(ITEMIMAGESELECTOR).get(4).click(); Thread.sleep(3000);
 * driver.findElements(SIZECHECKBOX).get(1).click(); Thread.sleep(4000); Select
 * quantityDropDown = new
 * Select(driver.findElement(By.cssSelector("#productVariantQty")));
 * quantityDropDown.selectByIndex(1); driver.findElement(ADDTOCART).click();
 * Thread.sleep(5000); driver.findElement(VIEWBASKET).click();
 * Thread.sleep(4000); driver.findElement(CLICKVIEWBASKET).click(); //view
 * basket and checkout Thread.sleep(3000);
 * driver.findElement(VIEWBASKET).click(); Thread.sleep(3000);
 * driver.findElement(CLICKVIEWBASKET).click(); //view basket checkout
 * Thread.sleep(5000); driver.findElement(PROCEEDTOCHECKOUTDOWN).click();
 * Thread.sleep(3000); driver.findElement(GUESTEMAIL).clear();
 * driver.findElement(GUESTEMAIL).sendKeys("veena.gokula@gmail.com");
 * Thread.sleep(4000); driver.findElement(GUESTCHECKOUT).click();
 * Thread.sleep(6000); }
 */

/*
 * @When("^select store and proceed to payment page$") public void
 * select_store_and_proceed_to_payment_page() throws Throwable {
 * driver.findElement(CLICKANDCOLLECT).click(); // click and collect icon click
 * Thread.sleep(5000); driver.findElement(CNCCONTINUE).click(); //continue
 * button Thread.sleep(3000); driver.findElement(CNCLOOKUP).clear(); // clickfor
 * geolocation driver.findElement(CNCLOOKUP).sendKeys("Tw3 1Ta");
 * driver.findElement(CNCLOOKUPBUTTON).click(); // lookup BUTTON
 * Thread.sleep(3000); driver.findElements(CNCSTORESELECT).get(0).click(); //
 * STORE NEAR BY SELECT Thread.sleep(3000);
 * driver.findElement(CNCSUMMARYBUTTON).click(); //continue }
 */

/*
 * @Then("^User will be routed to payment gateway page to proceed with payment$"
 * ) public void
 * user_will_be_routed_to_payment_gateway_page_to_proceed_with_payment() throws
 * Throwable { driver.findElement(CNCCONTINUEPAYMENT).click(); // contniue to
 * payment Thread.sleep(3000); driver.findElement(CNCPAYWITHCARD).click(); //
 * pay with a card Thread.sleep(5000); Select quantity1DropDown = new
 * Select(driver.findElement(CNCTITLEDROPDOWN)); //titledrop down
 * quantity1DropDown.selectByVisibleText("Mrs"); Thread.sleep(3000);
 * driver.findElement(CNCFIRSTNAME).clear();
 * driver.findElement(CNCFIRSTNAME).sendKeys("Veena");
 * driver.findElement(CNCSURNAME).clear();
 * driver.findElement(CNCSURNAME).sendKeys("Gokula");
 * driver.findElement(CNCADRESSPOSTCODE).sendKeys("TW31TA");
 * driver.findElement(CNCFINDADRESS).click(); // FindAddress Thread.sleep(3000);
 * Select quantity2DropDown = new Select(driver.findElement(CNCADRESSDROPDOWN));
 * quantity2DropDown.
 * selectByVisibleText("Flat 56 Madison Heights 17-27 High Street");
 * Thread.sleep(2000); driver.findElement(CNCTERMSANDCONDITIONS).click();
 * //click terms and conditions
 * Assert.assertEquals("I accept the Terms and conditions and Privacy policy ",
 * driver.findElement(By.cssSelector(".ln-c-form-option__label")).getText());
 * 
 * driver.findElement(CNCCARDPAYMENTCONTNIUE).click();
 * 
 * }
 */

// Home delivery

/*
 * @Given("^I am in checkout page as a guest in home delivery page$") public
 * void i_am_in_checkout_page_as_a_guest_in_home_delivery_page() throws
 * Throwable { driver.findElement(WOMENLINK).click(); Thread.sleep(3000);
 * driver.findElement(NEWARRIVALS).click(); Thread.sleep(3000);
 * driver.findElements(ITEMIMAGESELECTOR).get(3).click(); Thread.sleep(3000);
 * driver.findElements(SIZECHECKBOX).get(1).click(); Thread.sleep(4000); Select
 * quantityDropDown = new
 * Select(driver.findElement(By.cssSelector("#productVariantQty")));
 * quantityDropDown.selectByIndex(1); driver.findElement(ADDTOCART).click();
 * Thread.sleep(5000); driver.findElement(VIEWBASKET).click();
 * Thread.sleep(4000); driver.findElement(CLICKVIEWBASKET).click(); //view
 * basket and checkout Thread.sleep(3000);
 * driver.findElement(VIEWBASKET).click(); Thread.sleep(3000);
 * driver.findElement(CLICKVIEWBASKET).click(); //view basket checkout
 * Thread.sleep(5000); driver.findElement(PROCEEDTOCHECKOUTDOWN).click();
 * Thread.sleep(3000); driver.findElement(GUESTEMAIL).clear();
 * driver.findElement(GUESTEMAIL).sendKeys("veena.gokula@gmail.com");
 * driver.findElement(GUESTCHECKOUT).click(); Thread.sleep(5000);
 * driver.findElement(HOMEDELIVERY).click();
 * 
 * }
 */

/*
 * @When("^I select standard delivery and proceed to payment through card$")
 * public void i_select_standard_delivery_and_proceed_to_payment_through_card()
 * throws Throwable { Thread.sleep(4000);
 * driver.findElement(HDCONTINUE).click(); // continue button after home
 * delivery Select quantity1DropDown = new
 * Select(driver.findElement(By.cssSelector("select[name='titleCode']")));
 * quantity1DropDown.selectByVisibleText("Mrs"); Thread.sleep(3000);
 * driver.findElement(HDFIRSTNAME).clear();
 * driver.findElement(HDFIRSTNAME).sendKeys("Veena");
 * driver.findElement(HDLASTNAME).clear();
 * driver.findElement(HDLASTNAME).sendKeys("Gokula");
 * driver.findElement(HDPOSTCODE).sendKeys("TW31TA");
 * driver.findElement(HDFINDADRESS).click(); //FindAdress 
 * Thread.sleep(3000);
 * Select quantity2DropDown = new Select(driver.findElement(HDADRESSDROPDOWN));//adress drop down 
 *  selection quantity2DropDown.
 * selectByVisibleText("Flat 56 Madison Heights 17-27 High Street");
 * driver.findElement(HDBILLINGCHECKBOX).click(); // use as billing adress
 * driver.findElement(HDADRESSCONTINUE).click(); Thread.sleep(3000);
 * driver.findElement(HDSTANDARDDELIVERY).click(); // standard delivery
 * driver.findElement(HDSDCONTINUE).click(); //continue button }
 */

/*
 * @Then("^User payment will be sucessful$") public void
 * user_payment_will_be_sucessful() throws Throwable {
 * driver.findElement(HDCONTINUEPAYMENT).click(); Thread.sleep(3000);
 * driver.findElement(HDPAYWITHCARD).click(); // pay with card
 * Thread.sleep(3000);
 * //driver.findElement(By.cssSelector("label[for='contactPreferencesId']")).
 * click(); // click if we dnt wana contact you Thread.sleep(3000);
 * driver.findElement(HDTERMSANDCONDITIONS).click(); //click terms and
 * conditions
 * 
 * Assert.assertEquals("Terms and conditions and Privacy policy *",
 * driver.findElement(By.cssSelector(".ln-u-h6")).getText());
 * 
 * driver.findElement(HDCARDPAYMENTCONTINUE).click(); // continue to payement
 * 
 * }
 */

// End To End Scenarios

//@Given("^I am in click and collect checkout page as a guest$")
//public void i_am_in_click_and_collect_checkout_page_as_a_guest() throws Throwable {
//	System.setProperty("webdriver.chrome.driver", "C:\\Selenium-API\\ChromeDriver\\chromedriver.exe");
//	driver = new ChromeDriver();
//	driver.get(baseURL);
//	driver.manage().window().maximize();
//	Assert.assertEquals("https://tuclothing.sainsburys.co.uk/", driver.getCurrentUrl());
//	
//	driver.findElement(WOMENLINK).click();
//	Thread.sleep(3000);
//	driver.findElement(NEWARRIVALS).click();
//	Thread.sleep(3000);
//	driver.findElements(ITEMIMAGESELECTOR).get(4).click();
//	Thread.sleep(3000);
//	driver.findElements(SIZECHECKBOX).get(1).click();
//	Thread.sleep(4000);
//	Select quantityDropDown = new Select(driver.findElement(By.cssSelector("#productVariantQty")));
//	quantityDropDown.selectByIndex(1);
//	driver.findElement(ADDTOCART).click();
//	Thread.sleep(5000);
//	driver.findElement(VIEWBASKET).click();
//	Thread.sleep(4000);
//	driver.findElement(CLICKVIEWBASKET).click();    //view basket and checkout
//	Thread.sleep(3000);
//	driver.findElement(VIEWBASKET).click();
//	Thread.sleep(3000);
//	driver.findElement(CLICKVIEWBASKET).click();  //view basket checkout
//	Thread.sleep(5000);
//	driver.findElement(PROCEEDTOCHECKOUTDOWN).click();
//	Thread.sleep(3000);
//	driver.findElement(GUESTEMAIL).clear();
//	driver.findElement(GUESTEMAIL).sendKeys("veena.gokula@gmail.com");
//	Thread.sleep(4000);
//	driver.findElement(GUESTCHECKOUT).click();
//}

//@When("^I select nearby store and proceed to summary page$")
//public void i_select_nearby_store_and_proceed_to_summary_page() throws Throwable {
//	Thread.sleep(5000);
//	driver.findElement(CLICKANDCOLLECT).click();					 // click and collect icon click
//	Thread.sleep(3000);
//	driver.findElement(CNCCONTINUE).click();	//continue button
//	Thread.sleep(3000);
//	driver.findElement(CNCLOOKUP).clear();								// clickfor geolocation
//	driver.findElement(CNCLOOKUP).sendKeys("Tw3 1Ta");
//	driver.findElement(CNCLOOKUPBUTTON).click();		// lookup BUTTON
//	Thread.sleep(3000);
//	driver.findElements(CNCSTORESELECT).get(0).click(); // STORE NEAR BY SELECT
//	Thread.sleep(3000);
//	driver.findElement(CNCSUMMARYBUTTON).click();		//continue
//	
//}

/*
 * @Then("^I am routed to payment gateway page to proceed with payment$") public
 * void i_am_routed_to_payment_gateway_page_to_proceed_with_payment() throws
 * Throwable { driver.findElement(CNCCONTINUEPAYMENT).click(); // contniue to
 * payment Thread.sleep(3000); driver.findElement(CNCPAYWITHCARD).click(); //
 * pay with a card Thread.sleep(5000); Select quantity1DropDown = new
 * Select(driver.findElement(CNCTITLEDROPDOWN)); //titledrop down
 * quantity1DropDown.selectByVisibleText("Mrs"); Thread.sleep(3000);
 * driver.findElement(CNCFIRSTNAME).clear();
 * driver.findElement(CNCFIRSTNAME).sendKeys("Veena");
 * driver.findElement(CNCSURNAME).clear();
 * driver.findElement(CNCSURNAME).sendKeys("Gokula");
 * driver.findElement(CNCADRESSPOSTCODE).sendKeys("TW31TA");
 * driver.findElement(CNCFINDADRESS).click(); // FindAddress Thread.sleep(3000);
 * Select quantity2DropDown = new Select(driver.findElement(CNCADRESSDROPDOWN));
 * quantity2DropDown.
 * selectByVisibleText("Flat 56 Madison Heights 17-27 High Street");
 * Thread.sleep(2000); driver.findElement(CNCTERMSANDCONDITIONS).click();
 * //click terms and conditions
 * Assert.assertEquals("I accept the Terms and conditions and Privacy policy ",
 * driver.findElement(By.cssSelector(".ln-c-form-option__label")).getText());
 * 
 * driver.findElement(CNCCARDPAYMENTCONTNIUE).click(); }
 */
//@Given("^I am in home delivery checkout page as a guest$")
//public void i_am_in_home_delivery_checkout_page_as_a_guest() throws Throwable {
//	driver.findElement(WOMENLINK).click();
//	Thread.sleep(3000);
//	driver.findElement(NEWARRIVALS).click();
//	Thread.sleep(3000);
//	driver.findElements(ITEMIMAGESELECTOR).get(3).click();
//	Thread.sleep(3000);
//	driver.findElements(SIZECHECKBOX).get(1).click();
//	Thread.sleep(4000);
//	Select quantityDropDown = new Select(driver.findElement(By.cssSelector("#productVariantQty")));
//	quantityDropDown.selectByIndex(1);
//	driver.findElement(ADDTOCART).click();
//	Thread.sleep(5000);
//	driver.findElement(VIEWBASKET).click();
//	Thread.sleep(4000);
//	driver.findElement(CLICKVIEWBASKET).click();    //view basket and checkout
//	Thread.sleep(3000);
//	driver.findElement(VIEWBASKET).click();
//	Thread.sleep(3000);
//	driver.findElement(CLICKVIEWBASKET).click();  //view basket checkout
//	Thread.sleep(5000);
//	driver.findElement(PROCEEDTOCHECKOUTDOWN).click();
//	Thread.sleep(3000);
//	driver.findElement(GUESTEMAIL).clear();
//	driver.findElement(GUESTEMAIL).sendKeys("veena.gokula@gmail.com");
//	driver.findElement(GUESTCHECKOUT).click();
//	Thread.sleep(5000);
//	driver.findElement(HOMEDELIVERY).click();
//}

//@When("^I select standard delivery and continue payment through card$")
//public void i_select_standard_delivery_and_continue_payment_through_card() throws Throwable {
//	Thread.sleep(4000);
//	driver.findElement(HDCONTINUE).click();						// continue button after home delivery
//	Select quantity1DropDown = new Select(driver.findElement(By.cssSelector("select[name='titleCode']")));
//	quantity1DropDown.selectByVisibleText("Mrs");
//	Thread.sleep(3000);
//	driver.findElement(HDFIRSTNAME).clear();								
//	driver.findElement(HDFIRSTNAME).sendKeys("Veena");
//	driver.findElement(HDLASTNAME).clear();
//	driver.findElement(HDLASTNAME).sendKeys("Gokula");
//	driver.findElement(HDPOSTCODE).sendKeys("TW31TA");
//	driver.findElement(HDFINDADRESS).click(); 					 //FindAdress
//	Thread.sleep(3000);
//	Select quantity2DropDown = new Select(driver.findElement(HDADRESSDROPDOWN)); 		//adress drop down selection
//	quantity2DropDown.selectByVisibleText("Flat 56 Madison Heights 17-27 High Street");
//	driver.findElement(HDBILLINGCHECKBOX).click();								// use as billing adress
//	driver.findElement(HDADRESSCONTINUE).click();	
//	Thread.sleep(3000);	
//	driver.findElement(HDSTANDARDDELIVERY).click();						// standard delivery
//	driver.findElement(HDSDCONTINUE).click();						//continue button
//}

//@Then("^User payment is successful$")
//public void user_payment_is_successful() throws Throwable {
//	driver.findElement(HDCONTINUEPAYMENT).click();
//	Thread.sleep(3000);
//	driver.findElement(HDPAYWITHCARD).click();							// pay with card
//	Thread.sleep(3000);
//	//driver.findElement(By.cssSelector("label[for='contactPreferencesId']")).click();       	  //   click if we dnt wana contact you
//	Thread.sleep(3000);
//	driver.findElement(HDTERMSANDCONDITIONS).click(); 	              //click terms and conditions
//	
//	Assert.assertEquals("Terms and conditions and Privacy policy *", driver.findElement(By.cssSelector(".ln-u-h6")).getText());
//	
//	driver.findElement(HDCARDPAYMENTCONTINUE).click(); 							  // continue to payement
//
//}
















//	@After
//	public void close() {
//		driver.close();
//	}
}
