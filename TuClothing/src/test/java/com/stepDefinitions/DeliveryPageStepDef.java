package com.stepDefinitions;

import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.Select;

import com.runner.BaseClass;

import cucumber.api.java.en.When;

public class DeliveryPageStepDef extends BaseClass {

	@When("^select store and proceed to payment page$")
	public void select_store_and_proceed_to_payment_page() throws Throwable {
		deliveryPage.verifyPaymentNavigation();
	}

	@When("^I select standard delivery and proceed to payment through card$")
	public void i_select_standard_delivery_and_proceed_to_payment_through_card() throws Throwable {
		deliveryPage.guestCheckOutHomeDeliveryToPayment();
	}

	@When("^I select nearby store and proceed to summary page$")
	public void i_select_nearby_store_and_proceed_to_summary_page() throws Throwable {
		deliveryPage.verifyPaymentNavigation();
	}

	@When("^I select standard delivery and continue payment through card$")
	public void i_select_standard_delivery_and_continue_payment_through_card() throws Throwable {
		deliveryPage.guestCheckOutEndToEndDeliveryProcess();
	}

}
