package com.stepDefinitions;

import com.runner.BaseClass;

import cucumber.api.java.en.Given;

public class CheckOutStepDef extends BaseClass{
	
	@Given("^I am in checkout page as a guest in click and collect$")
	public void i_am_in_checkout_page_as_a_guest_in_click_and_collect() throws Throwable {
		checkOutPage.verifyNavigation();
	}
	
	@Given("^I am in checkout page as a guest in home delivery page$")
	public void i_am_in_checkout_page_as_a_guest_in_home_delivery_page() throws Throwable {
		checkOutPage.homeDeliveryAsGuestCheckout();

	}
	@Given("^I am in click and collect checkout page as a guest$")  // end to end
	public void i_am_in_click_and_collect_checkout_page_as_a_guest() throws Throwable {
		checkOutPage.guestCheckOutEndToEnd();
	}

	@Given("^I am in home delivery checkout page as a guest$")
	public void i_am_in_home_delivery_checkout_page_as_a_guest() throws Throwable {
		checkOutPage.homeDeliveryEndToEndCheckOutProcess();
		
	}
}
