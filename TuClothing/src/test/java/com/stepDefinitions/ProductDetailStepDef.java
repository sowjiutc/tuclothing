package com.stepDefinitions;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

import com.runner.BaseClass;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class ProductDetailStepDef extends BaseClass {
	
	@Given("^User on product details page$")
	public void user_on_product_details_page() throws Throwable {
		productDetailPage.userOnProductDetailPage();
	}
	@When("^User entered valid data$")  // recaptcha prob cannot login
	public void user_entered_valid_data() throws Throwable {
	}
	@When("^select product and add to basket$")   // 
	public void select_product_and_add_to_basket() throws Throwable {
		productDetailPage.addProductToBasket();
	}
	@Then("^User  allowed to add item to basket$")
	public void user_allowed_to_add_item_to_basket() throws Throwable {
		productDetailPage.addProductToBasketAssert();
	}
	@When("^User selected a product and add to basket$")
	public void user_selected_a_product_and_add_to_basket() throws Throwable {
		productDetailPage.addProductToBasketWithOutLogin();
	}
	@Then("^User is allowed without login to add items to basket$")
	public void user_is_allowed_without_login_to_add_items_to_basket() throws Throwable {
		productDetailPage.addProductToBasketWithOutLoginAssert();
	}
	@When("^User selected multiple products and add to basket$")
	public void user_selected_multiple_products_and_add_to_basket() throws Throwable {
		productDetailPage.multipleProductsWithOutLogin();
}
	@Then("^User is allowed without login to add multiple items to basket$")
	public void user_is_allowed_without_login_to_add_multiple_items_to_basket() throws Throwable {
		productDetailPage.multipleProductsWithOutLoginAssert();
	}
	@When("^User login and select multiple products and add to basket$")  // recaptcha prob so did not write code for login
	public void user_login_and_select_multiple_products_and_add_to_basket() throws Throwable {
		productDetailPage.multipleProductsWithLogin();
	}
	@Then("^User allowed to add products to basket$")
	public void user_allowed_to_add_products_to_basket() throws Throwable {
		productDetailPage.multipleProductsWithLoginAssert();
	}
	@When("^User select to remove products from basket$")
	public void user_select_to_remove_products_from_basket() throws Throwable {
		productDetailPage.guestRemovingItemsFromBasket();
	}
	@Then("^product should be removed from the basket$")
	public void product_should_be_removed_from_the_basket() throws Throwable {
		productDetailPage.guestRemovingItemsFromBasketAssert();
	}
}
