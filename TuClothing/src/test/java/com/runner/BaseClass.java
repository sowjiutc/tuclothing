package com.runner;

import org.junit.Assert;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import com.driver.Action;
import com.driver.Get;
import com.pages.BasketPage;
import com.pages.CheckOutPage;
import com.pages.DeliveryPage;
import com.pages.HomePage;
import com.pages.LoginRegisterPage;
import com.pages.PaymentPage;
import com.pages.ProductConstants;
import com.pages.ProductDetailPage;
import com.pages.SearchResultPage;
import com.pages.StoreLocatorPage;

public class BaseClass {
	
	public static WebDriver driver;
	public static HomePage homePage =new HomePage();
	public static SearchResultPage searchResultPage = new SearchResultPage();
	public static LoginRegisterPage loginRegisterPage = new LoginRegisterPage();
	public static StoreLocatorPage storeLocatorPage = new StoreLocatorPage();
	public static Get get = new Get();
	public static Action action = new Action();
	public static ProductDetailPage productDetailPage = new ProductDetailPage();
	public static BasketPage basketPage = new BasketPage();
	public static DeliveryPage deliveryPage = new DeliveryPage();
	public static PaymentPage paymentPage = new PaymentPage();
	public static CheckOutPage checkOutPage = new CheckOutPage();
	
	

	public static void initilizeDriver() {
		System.setProperty("webdriver.chrome.driver", "C:\\Selenium-API\\ChromeDriver\\chromedriver.exe");
		driver = new ChromeDriver();
		driver.get(ProductConstants.BASE_URL);
		driver.manage().window().maximize();
		Assert.assertEquals("https://tuclothing.sainsburys.co.uk/", driver.getCurrentUrl());
	}
	
}
