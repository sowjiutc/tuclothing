package com.driver;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

import com.runner.BaseClass;

public class Action extends BaseClass {

	public void updateElement(By elementName, String keyword) {
		driver.findElement(elementName).clear();
		driver.findElement(elementName).sendKeys(keyword);
	}

	public void clear(By elementName) {
		driver.findElement(elementName).clear();
	}

	public void sendkeys(By elementName, String text) {
		driver.findElement(elementName).sendKeys(text);
	}

	public void clickOnTheElement(By elementName) {
		driver.findElement(elementName).click();
	}

	public void dropdownIndex(By elementName, int index) {
		Select DropDown = new Select(driver.findElement(elementName));
		DropDown.selectByIndex(index);
	}
	
	public void dropdownVisbleText(By elementName, String visibleText) {
		Select DropDown = new Select(driver.findElement(elementName));
		DropDown.selectByVisibleText(visibleText);
	}

	public void clickOnTheElementWithIndex(By elementName, int index) {
		driver.findElements(elementName).get(index).click();
		/*
		 * System.out.println("elementName::"+elementName);
		 * System.out.println("index::"+index); List<WebElement> list =
		 * driver.findElements(elementName); System.out.println("size::"+list.size());
		 */

	}
}
