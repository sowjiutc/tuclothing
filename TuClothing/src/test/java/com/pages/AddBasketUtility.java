package com.pages;

import org.junit.Assert;
import org.openqa.selenium.By;

import com.driver.Action;
import com.runner.BaseClass;

public class AddBasketUtility extends BaseClass {

	protected static void productSelectActivities(By imageItem,int imageIndex) throws InterruptedException{
		
		action.clickOnTheElement(ProductConstants.WOMENLINK);
		Assert.assertEquals("Womens", get.getElementText(ProductConstants.ASSERTWOMENS));
		Thread.sleep(3000);
		action.clickOnTheElement(ProductConstants.NEWARRIVALS);
		Thread.sleep(3000);
		Assert.assertEquals("Womens Clothing New In", get.getElementText(ProductConstants.ASSERTH1));
		Thread.sleep(4000);
		action.clickOnTheElementWithIndex(imageItem, imageIndex); // First product of the page ITEMIMAGESELECTOR,2
		Thread.sleep(3000);
		action.clickOnTheElementWithIndex(ProductConstants.SIZECHECKBOX, 1);
		Thread.sleep(3000);
		action.dropdownIndex(ProductConstants.PRODUCTQUANTITY, 4);
		action.clickOnTheElement(ProductConstants.ADDTOCART);
		Thread.sleep(3000);
	}
	
	protected static void guestCheckOutBasket() throws InterruptedException {
		
		initilizeDriver();
		
		productSelectActivities(ProductConstants.ITEMIMAGESELECTOR,4);  //productSelectActivities calling (add items to basket)
		
		Thread.sleep(5000);
		action.clickOnTheElement(ProductConstants.VIEWBASKET);
		Thread.sleep(4000);
		action.clickOnTheElement(ProductConstants.CLICKVIEWBASKET); //view basket and checkout
		Thread.sleep(3000);
		action.clickOnTheElement(ProductConstants.VIEWBASKET);
		Thread.sleep(3000);
		action.clickOnTheElement(ProductConstants.CLICKVIEWBASKET); //view basket checkout
		Thread.sleep(5000);
		action.clickOnTheElement(ProductConstants.PROCEEDTOCHECKOUTDOWN);
		Thread.sleep(3000);
		action.updateElement(ProductConstants.GUESTEMAIL, "veena.gokula@gmail.com");
		Thread.sleep(4000);
		action.clickOnTheElement(ProductConstants.GUESTCHECKOUT);
		Thread.sleep(6000);
	}
}
