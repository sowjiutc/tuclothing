package com.pages;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.Select;

import com.runner.BaseClass;

public class PaymentPage extends BaseClass {

	public void verifyPaymentNavigationAssert() throws InterruptedException {

		action.clickOnTheElement(ProductConstants.CNCCONTINUEPAYMENT); // contniue to payment
		Thread.sleep(3000);
		action.clickOnTheElement(ProductConstants.CNCPAYWITHCARD); // pay with a card
		Thread.sleep(5000);

		action.dropdownVisbleText(ProductConstants.CNCTITLEDROPDOWN, "Mrs");// titledrop down

		Thread.sleep(3000);
		action.updateElement(ProductConstants.CNCFIRSTNAME, "Veena");

		action.updateElement(ProductConstants.CNCSURNAME, "Gokula");

		action.updateElement(ProductConstants.CNCADRESSPOSTCODE, "TW31TA");

		action.clickOnTheElement(ProductConstants.CNCFINDADRESS); // FindAddress
		Thread.sleep(3000);
		action.dropdownVisbleText(ProductConstants.CNCADRESSDROPDOWN, "Flat 56 Madison Heights 17-27 High Street");
		Thread.sleep(2000);
		action.clickOnTheElement(ProductConstants.CNCTERMSANDCONDITIONS); // click terms and conditions
		Assert.assertEquals("I accept the Terms and conditions and Privacy policy ",
				get.getElementText((ProductConstants.ASSEERTPRIVACYPOLICY)));
		action.clickOnTheElement(ProductConstants.CNCCARDPAYMENTCONTNIUE);

	}

	public void paymentCANDCProcessAssert() throws InterruptedException {
		action.clickOnTheElement(ProductConstants.HDCONTINUEPAYMENT);
		Thread.sleep(3000);
		action.clickOnTheElement(ProductConstants.HDPAYWITHCARD); // pay with card
		Thread.sleep(3000);
		action.clickOnTheElement(ProductConstants.HDTERMSANDCONDITIONS); // click terms and conditions
		Assert.assertEquals("Terms and conditions and Privacy policy *",
				get.getElementText((ProductConstants.ASSERTTANDCONDITION)));

		action.clickOnTheElement(ProductConstants.HDCARDPAYMENTCONTINUE); // continue to payement

	}

	public void homeDeliveryPaymentProcess() throws InterruptedException {

		action.clickOnTheElement(ProductConstants.CNCCONTINUEPAYMENT); // contniue to payment
		Thread.sleep(3000);
		action.clickOnTheElement(ProductConstants.CNCPAYWITHCARD); // pay with a card
		Thread.sleep(5000);

		action.dropdownVisbleText(ProductConstants.CNCTITLEDROPDOWN, "Mrs");// titledrop down

		Thread.sleep(3000);
		action.updateElement(ProductConstants.CNCFIRSTNAME, "Veena");

		action.updateElement(ProductConstants.CNCSURNAME, "Gokula");

		action.updateElement(ProductConstants.CNCADRESSPOSTCODE, "TW31TA");

		action.clickOnTheElement(ProductConstants.CNCFINDADRESS); // FindAddress
		Thread.sleep(3000);
		action.dropdownVisbleText(ProductConstants.CNCADRESSDROPDOWN, "Flat 56 Madison Heights 17-27 High Street");
		Thread.sleep(2000);
		action.clickOnTheElement(ProductConstants.CNCTERMSANDCONDITIONS); // click terms and conditions
		Assert.assertEquals("I accept the Terms and conditions and Privacy policy ",
				get.getElementText((ProductConstants.ASSEERTPRIVACYPOLICY)));
		action.clickOnTheElement(ProductConstants.CNCCARDPAYMENTCONTNIUE);
	}

	public void guestCheckOutEndToEndAssert() throws InterruptedException {
		paymentCANDCProcessAssert();
	}
}
