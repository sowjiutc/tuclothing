package com.pages;

import com.runner.BaseClass;

public class DeliveryPage extends BaseClass {

	public void verifyPaymentNavigation() throws InterruptedException {

		action.clickOnTheElement(ProductConstants.CNCDELIVERY);
		Thread.sleep(5000);
		action.clickOnTheElement(ProductConstants.CNCDELIVERY); // click and collect icon click
		Thread.sleep(3000);
		action.clickOnTheElement(ProductConstants.CNCCONTINUE); // continue button
		Thread.sleep(3000);
		action.updateElement(ProductConstants.CNCLOOKUP, "Tw3 1Ta"); // click for geolocation
		Thread.sleep(3000);
		action.clickOnTheElement(ProductConstants.CNCLOOKUPBUTTON); // lookup BUTTON
		Thread.sleep(3000);
		action.clickOnTheElement(ProductConstants.CNCLOOKUPBUTTON);
		Thread.sleep(3000);
		action.clickOnTheElementWithIndex(ProductConstants.CNCSTORESELECT, 0); // STORE NEAR BY SELECT
		Thread.sleep(3000);
		action.clickOnTheElement(ProductConstants.CNCSUMMARYBUTTON); // continue

	}

	public void guestCheckOutHomeDeliveryToPayment() throws InterruptedException {
		Thread.sleep(4000);
		action.clickOnTheElement(ProductConstants.HDCONTINUE); // continue button after home delivery
		action.dropdownVisbleText(ProductConstants.HDTITLECODE, "Mrs");
		Thread.sleep(3000);
		action.updateElement(ProductConstants.HDFIRSTNAME, "Veena");
		action.updateElement(ProductConstants.HDLASTNAME, "Gokula");
		action.updateElement(ProductConstants.HDPOSTCODE, "TW31TA");
		action.clickOnTheElement(ProductConstants.HDFINDADRESS);
		Thread.sleep(3000);
		action.dropdownVisbleText(ProductConstants.HDADRESSDROPDOWN, "Flat 56 Madison Heights 17-27 High Street");
		Thread.sleep(3000);
		action.clickOnTheElement(ProductConstants.HDBILLINGCHECKBOX); // use as billing adress
		Thread.sleep(3000);
		action.clickOnTheElement(ProductConstants.HDADRESSCONTINUE);
		Thread.sleep(3000);
		action.clickOnTheElement(ProductConstants.HDSTANDARDDELIVERY); // standard delivery
		action.clickOnTheElement(ProductConstants.HDSDCONTINUE); // continue button

	}

	public void guestCheckOutEndToEndDeliveryProcess() throws InterruptedException {
		guestCheckOutHomeDeliveryToPayment();
	}
}
