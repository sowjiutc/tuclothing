package com.pages;

import com.runner.BaseClass;

public class CheckOutPage extends BaseClass {

	public void verifyNavigation() throws InterruptedException {// given
		AddBasketUtility.guestCheckOutBasket();

	}

	public void homeDeliveryAsGuestCheckout() throws InterruptedException {
		AddBasketUtility.guestCheckOutBasket();
		action.clickOnTheElement(ProductConstants.HOMEDELIVERY);

	}

	public void guestCheckOutEndToEnd() throws InterruptedException {
		AddBasketUtility.guestCheckOutBasket();

	}
	public void homeDeliveryEndToEndCheckOutProcess() throws InterruptedException {
		
		AddBasketUtility.guestCheckOutBasket();
		Thread.sleep(5000);
		action.clickOnTheElement(ProductConstants.HOMEDELIVERY);
		
	}

}
