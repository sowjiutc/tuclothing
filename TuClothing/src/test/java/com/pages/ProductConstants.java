package com.pages;

import org.openqa.selenium.By;

public interface ProductConstants {

	By WOMENLINK = By.linkText("Women");
	By NEWARRIVALS = By.linkText("New Arrivals");
	By SIZECHECKBOX = By.cssSelector(".selectable");
	By PRODUCTQUANTITYDROPDOWN = By.cssSelector("#productVariantQty");
	By ADDTOCART = By.cssSelector("#AddToCart");
	By ITEMIMAGESELECTOR = By.cssSelector(".image-component img");
	By MINIBASKET = By.cssSelector(".nav_cart_text");
	By MINIVIEWBASKET = By.cssSelector("a[href='/cart']");
	By VIEWBASKET = By.cssSelector("#basket-title");
	By REMOVEDCARTITEM = By.cssSelector(".removeCartItem");
	By CLICKVIEWBASKET = By.cssSelector("a[class='ln-c-button ln-c-button--primary']");
	By REMOVEPRODUCT = By.cssSelector("#RemoveProduct_0");
	By BASKETEMPTY = By.cssSelector(".cart_icon");
	By SIZEDISABLED = By.cssSelector("div[data-value='137621027']");
	By QUANTITYUPDATE = By.cssSelector("a[class='basketUpdateProd updateQuantityProduct']");
	By ASSERTWOMENS = By.cssSelector(".ln-u-border-bottom--double");
	By ASSERTH1 = By.cssSelector("h1");
	By PRODUCTQUANTITY = By.cssSelector("#productVariantQty");
	By ASSERTPRODQUANTITY = By.cssSelector(".prod_quantity");
	By ADDCART = By.cssSelector(".cart_icon");

	String BASE_URL = "https://tuclothing.sainsburys.co.uk/";
	// Add product to basket

	// Basket CheckOut
	By CHECKOUTBUTTON = By.cssSelector("a[data-testid='checkoutButton']");
	By SHOWPROMO = By.cssSelector("#showPromo");
	By VOUCHERPAY = By.cssSelector("#voucher_voucherCode");
	By VOUCHERAPPLY = By.cssSelector("button[class='ln-c-button ln-c-button--secondary']");
	By PROCEEDTOCHECKOUTUP = By.cssSelector("#basketButtonTop");
	By PROCEEDTOCHECKOUTDOWN = By.cssSelector("#basketButtonBottom");
	By GUESTEMAIL = By.cssSelector("#guest_email");
	By GUESTCHECKOUT = By.cssSelector("button[data-testid='guest_checkout']"); // guest checkout
	By QUANTITYUPDATEDROPDOWN = By.cssSelector("#quantity0");
	By UPDATEGLOBAL = By.cssSelector("#globalMessages");
	By CART_ITEMS_TEXT = By.cssSelector(".cartItemsText");

	// Click and Collect
	By CNCDELIVERY = By.cssSelector("label[for='CLICK_AND_COLLECT']"); // CLICK N COLLECTs
	By CNCCONTINUE = By.cssSelector(".ln-c-button.ln-c-button--primary");
	By CNCLOOKUP = By.cssSelector("#lookup");
	By CNCLOOKUPBUTTON = By.cssSelector("span[data-testid='button-text']");
	By CNCSTORESELECT = By.cssSelector("button[data-testid='select-store']");
	By CNCSUMMARYBUTTON = By.cssSelector("button[data-testid='submit-button']");
	By CNCCONTINUEPAYMENT = By.cssSelector("button[data-testid='continueToPayment']");
	By CNCPAYWITHCARD = By.linkText("Pay with a card");
	By CNCTITLEDROPDOWN = By.cssSelector(".ln-c-form-group.ln-u-push-bottom .ln-c-select");
	By CNCFIRSTNAME = By.cssSelector("input[name='newFirstName']");
	By CNCSURNAME = By.cssSelector("input[name='newSurname']");
	By CNCADRESSPOSTCODE = By.cssSelector("#addressPostcode");
	By CNCFINDADRESS = By.cssSelector("button[class='ln-c-button ln-c-button--primary']");
	By CNCADRESSDROPDOWN = By.cssSelector("#addressListView");
	By CNCTERMSANDCONDITIONS = By.cssSelector(".ln-c-form-option__label");
	By CNCCARDPAYMENTCONTNIUE = By.cssSelector("#contPayment");

	// Home Delivery
	By HOMEDELIVERY = By.cssSelector("label[for='HOME_DELIVERY']");
	By HDCONTINUE = By.cssSelector(".ln-c-button.ln-c-button--primary"); // Homedelivery continue button
	By HDFIRSTNAME = By.cssSelector("input[name='firstName']");
	By HDLASTNAME = By.cssSelector("input[name='lastName']");
	By HDPOSTCODE = By.cssSelector("#addressPostcode");
	By HDFINDADRESS = By.cssSelector(".address-lookup.ln-u-push-bottom");
	By HDADRESSDROPDOWN = By.cssSelector("#addressListView");
	By HDBILLINGCHECKBOX = By.cssSelector(".ln-c-form-option__label");
	By HDADRESSCONTINUE = By.cssSelector("#continue");
	By HDSTANDARDDELIVERY = By.cssSelector("label[for='standard-delivery']");
	By HDSDCONTINUE = By.cssSelector("input[data-testid='continue']"); // HOMEDELIVERY STANDARD DELIVERY
																		// CONTNIUE
	By HDCONTINUEPAYMENT = By.cssSelector("button[data-testid='continueToPayment']");
	By HDPAYWITHCARD = By.cssSelector("a[data-testid='payWithCard']");
	By HDTERMSANDCONDITIONS = By.cssSelector(".ln-c-form-option__label");
	By HDCARDPAYMENTCONTINUE = By.cssSelector("#contPayment");

	By HDTITLECODE = By.cssSelector("select[name='titleCode']");
	By ASSEERTPRIVACYPOLICY = By.cssSelector(".ln-c-form-option__label");
	By ASSERTTANDCONDITION = By.cssSelector(".ln-u-h6");
}
