package com.pages;

import java.util.Map;

import org.junit.Assert;
import org.openqa.selenium.By;

import com.runner.BaseClass;

import cucumber.api.DataTable;

public class LoginRegisterPage extends BaseClass {
	//login
	private static By EMAILID = By.cssSelector("#j_username");
	private static By EMAIL = By.cssSelector("#j_username");
	private static By PASSWORD = By.cssSelector("#j_password");
	private static By FORGOTPASSWORD = By.cssSelector("#forgot-password-link");
	private static By lOGINBUTTON = By.cssSelector("#submit-login");
	private static By HIDESHOW = By.cssSelector(".hide-show-button");
	private static By ASSERTLOGINWITHTU = By.cssSelector("#login-register-form .span-12.retuncustomer .loginFormHolder h3");
	private static By ASSERTREGISTERWITHTU=By.cssSelector(".span-12.last h2");
	

	// Register
	private static By REGISTERBUTTON = By.cssSelector(".regToggle");
	private static By REGISTEREMAIL = By.cssSelector("#register_email");
	private static By REGISTERTITLE = By.cssSelector("#register_title");
	private static By REGISTERFIRSTNAME = By.cssSelector("#register_firstName");
	private static By REGISTERLASTNAME = By.cssSelector("#register_lastName");
	private static By REGPASSWORD = By.cssSelector("#password");
	private static By REGCHECKPASSWORD = By.cssSelector("#register_checkPwd");
	private static By COMPLETEREGISTER = By.cssSelector("#submit-register");
	private static By REGISTERNECTARPOINTS = By.cssSelector("#regNectarPointsOne");
	private static By TERMSNCONDITIONS = By.linkText("terms and conditions");
	//private static By SHOW = By.linkText("Show");
	private static By SHOW = By.cssSelector(".hide-show-button");
	
	
	public void user_enter_valid_credentials(String email, String password) {
		 
		action.updateElement(EMAILID, email);
		action.updateElement(PASSWORD, password);

	}
	public void UserEnterValidCredentialsAssert() {
	Assert.assertEquals("Log in with your Tu details", get.getElementText(ASSERTLOGINWITHTU));
	System.out.println("Please check Recaptcha");
}
	public void user_provides_invalid_email_id_and_password() {
		action.updateElement(EMAILID, "sdgfagd@gmail.com");
		action.updateElement(EMAILID, "asdjgashkdSGK");
	}
	
	public void userWillGetErrorMessageAssert() {
	Assert.assertEquals("Log in with your Tu details", get.getElementText(ASSERTLOGINWITHTU));
		System.out.println("Please check Recaptcha");
	}
	public void user_provides_blank_email_id_and_password() {
		action.updateElement(EMAILID, " ");
		action.clickOnTheElement(PASSWORD);
	}
	public void usershouldseeerrorAssert() {
	Assert.assertEquals("Log in with your Tu details", get.getElementText(ASSERTLOGINWITHTU));
	System.out.println("Please check Recaptcha");
	}
	public void invalidIdAndValidPassword() {
		action.updateElement(EMAILID, "dfgadhfgad@gmail.com ");
		action.updateElement(PASSWORD, "sunshine1234");
	}
	public void invalidIdAndPasswordAssert() {
	Assert.assertEquals("Log in with your Tu details", get.getElementText(ASSERTLOGINWITHTU));
	System.out.println("Please check Recaptcha");
}
	public void invalidIdAndInvalidPassword() {
		action.updateElement(EMAILID, "FBAfjAFJ@gmail.com");
		action.updateElement(PASSWORD, "sadvasmbv");
}
	public void invalidIdInvalidPasswordAssert() {
	Assert.assertEquals("Log in with your Tu details",
			get.getElementText(ASSERTLOGINWITHTU));
	System.out.println("please check the fields below and try again");
}
	public void hideShowPassword () throws InterruptedException {
		action.updateElement(EMAILID, "Sunny@gmail.com");
		action.updateElement(PASSWORD, "sunshine1234");
		Thread.sleep(3000);
		action.clickOnTheElement(HIDESHOW);
}	
	public void hideShowPasswordAssert() {
	Assert.assertEquals("Log in with your Tu details",
			get.getElementText(ASSERTLOGINWITHTU));
	}
	public void LoginWithValidCredentialsAndLogout() {
		action.updateElement(EMAILID, "Sunny@gmail.com");
		action.updateElement(PASSWORD, "sunshine1234");
	}
	public void loginNLogOutClickBackButton() {
		Assert.assertEquals("Log in with your Tu details",
				get.getElementText(ASSERTLOGINWITHTU));
	}
	public void loginWithOutRecaptcha() {
		action.updateElement(EMAILID, "Sunny@gmail.com");
		action.updateElement(PASSWORD, "sunshine1234");
	}
	public void loginWithOutRecaptchaAssert() {
	Assert.assertEquals("Log in with your Tu details",
			get.getElementText(ASSERTLOGINWITHTU));
	}
	public void NavigateToRegistrationPage () throws InterruptedException {
	Thread.sleep(3000);
	action.clickOnTheElement(REGISTERBUTTON);
	driver.findElement(REGISTERBUTTON).click();
	Assert.assertEquals("Register with Tu", get.getElementText(ASSERTREGISTERWITHTU));
	}
	public void mandatoryFieldsEmptyAndRegister() {  //removed all clicks
		action.clickOnTheElement(REGISTERBUTTON);
		action.updateElement(REGISTEREMAIL, "");
		action.dropdownIndex(REGISTERTITLE, 3);   // drop down
		action.updateElement(REGISTERFIRSTNAME, "");
		action.updateElement(REGISTERLASTNAME, "");		
		action.updateElement(REGPASSWORD, "");
		action.updateElement(REGCHECKPASSWORD, "");
		action.updateElement(REGISTERNECTARPOINTS, "");
		action.clickOnTheElement(COMPLETEREGISTER);
	}
	public void mandatoryFieldsEmptyAndRegisterAssert() {
	Assert.assertEquals("Register with Tu", get.getElementText(ASSERTREGISTERWITHTU));
	System.out.println("please fill the blanks");
}
	
	public void registerWithOutEmail () {
		action.clickOnTheElement(REGISTERBUTTON);
		action.updateElement(REGISTEREMAIL, "");
		action.dropdownIndex(REGISTERTITLE, 3);   // drop down
		action.updateElement(REGISTERFIRSTNAME, "veena");
		action.updateElement(REGISTERLASTNAME, "gokula");		
		action.updateElement(REGPASSWORD, "man@1234");
		action.updateElement(REGCHECKPASSWORD, "man@1234");
		action.updateElement(REGISTERNECTARPOINTS, "12345678");
		action.clickOnTheElement(COMPLETEREGISTER);

	}
	public void registerWithOutEmailAssert() {
	Assert.assertEquals("Register with Tu", get.getElementText(ASSERTREGISTERWITHTU));
	System.out.println("please checkRecaptcha");
	}
	public void registerWiThInvalidEmail () {
		action.clickOnTheElement(REGISTERBUTTON);
		action.updateElement(REGISTEREMAIL, "veenagokulagmail.com");
		action.dropdownIndex(REGISTERTITLE, 3);   // drop down
		action.updateElement(REGISTERFIRSTNAME, "veena");
		action.updateElement(REGISTERLASTNAME, "gokula");		
		action.updateElement(REGPASSWORD, "man@1234");
		action.updateElement(REGCHECKPASSWORD, "man@1234");
		action.updateElement(REGISTERNECTARPOINTS, "12345678");
		action.clickOnTheElement(COMPLETEREGISTER);
	}
	public void registerWiThInvalidEmailAssert () {
	Assert.assertEquals("Register with Tu", get.getElementText(ASSERTREGISTERWITHTU));
	System.out.println("please checkRecaptcha");
	}
	public void registerWithSplCharectersAndNumbers () {        
		action.clickOnTheElement(REGISTERBUTTON);
		action.updateElement(REGISTEREMAIL, "veenagokula@gmail.com");
		action.dropdownIndex(REGISTERTITLE, 3);   // drop down
		action.updateElement(REGISTERFIRSTNAME, "�$%&$�");
		action.updateElement(REGISTERLASTNAME, "12345");		
		action.updateElement(REGPASSWORD, "man@1234");
		action.updateElement(REGCHECKPASSWORD, "man@1234");
		action.updateElement(REGISTERNECTARPOINTS, "12345678");
		action.clickOnTheElement(COMPLETEREGISTER);
	}
	public void registerWithSplCharNumbersAssert() {
	Assert.assertEquals("Register with Tu", get.getElementText(ASSERTREGISTERWITHTU));
	
	}
	public void registerWithDiffPasswords () {
		action.clickOnTheElement(REGISTERBUTTON);
		action.updateElement(REGISTEREMAIL, "veenagokula@gmail.com");
		action.dropdownIndex(REGISTERTITLE, 3);   // drop down
		action.updateElement(REGISTERFIRSTNAME, "veena");
		action.updateElement(REGISTERLASTNAME, "gokula");		
		action.updateElement(REGPASSWORD, "man@1234");
		action.updateElement(REGCHECKPASSWORD, "samyukth@1234");
		action.updateElement(REGISTERNECTARPOINTS, "12345678");
		action.clickOnTheElement(COMPLETEREGISTER);
}
	public void registerWithDiffPasswordsAssert () {
	Assert.assertEquals("Register with Tu", get.getElementText(ASSERTREGISTERWITHTU));
	
}
	public void registerpasswordWithMinimumlength() {
		action.clickOnTheElement(REGISTERBUTTON);
		action.updateElement(REGISTEREMAIL, "veenagokula@gmail.com");
		action.dropdownIndex(REGISTERTITLE, 3);   // drop down
		action.updateElement(REGISTERFIRSTNAME, "veena");
		action.updateElement(REGISTERLASTNAME, "gokula");		
		action.updateElement(REGPASSWORD, "man");
		action.updateElement(REGCHECKPASSWORD, "man");
		action.updateElement(REGISTERNECTARPOINTS, "12345678");
		action.clickOnTheElement(COMPLETEREGISTER);
	}
	public void registerpasswordWithMinimumlengthAssert() {
	Assert.assertEquals("Register with Tu", get.getElementText(ASSERTREGISTERWITHTU));
	}
	public void nectarCardNotMandatory () {
		action.clickOnTheElement(REGISTERBUTTON);
		action.updateElement(REGISTEREMAIL, "veenagokula@gmail.com");
		action.dropdownIndex(REGISTERTITLE, 3);   // drop down
		action.updateElement(REGISTERFIRSTNAME, "veena");
		action.updateElement(REGISTERLASTNAME, "gokula");		
		action.updateElement(REGPASSWORD, "man@1234");
		action.updateElement(REGCHECKPASSWORD, "man@1234");
		action.updateElement(REGISTERNECTARPOINTS, "");
		action.clickOnTheElement(COMPLETEREGISTER);
	}
	public void nectarCardNotMandatoryAssert () {
	Assert.assertEquals("Register with Tu", get.getElementText(ASSERTREGISTERWITHTU));
	}
	public void registerWithOutTermsAndConditions () {
		
		action.clickOnTheElement(REGISTERBUTTON);
		action.updateElement(REGISTEREMAIL, "veenagokula@gmail.com");
		action.dropdownIndex(REGISTERTITLE, 3);   // drop down
		action.updateElement(REGISTERFIRSTNAME, "veena");
		action.updateElement(REGISTERLASTNAME, "gokula");		
		action.updateElement(REGPASSWORD, "man@1234");
		action.updateElement(REGCHECKPASSWORD, "man@1234");
		action.updateElement(REGISTERNECTARPOINTS, "9876543");
		driver.findElement(By.linkText("terms and conditions")).click();
		
}
	public void registerWithOutTermsAndConditionsAssert() {
	Assert.assertEquals("Register with Tu", get.getElementText(ASSERTREGISTERWITHTU));
	System.out.println("Please correct the errors below.");
	}
	
	public void registerWithOutReCaptcha(DataTable userDetails) { 
	
		Map<String,String>registerDetails = userDetails.asMap(String.class, String.class);
		String Email = registerDetails.get("email");
		String FirstName = registerDetails.get("firstname");
		String Surname = registerDetails.get("surname");
		String Password = registerDetails.get("password");
		String ConfirmPassword = registerDetails.get("confirm Password");
		String NectarCard = registerDetails.get("nectarcard");
		
		action.clickOnTheElement(REGISTERBUTTON);
		action.updateElement(REGISTEREMAIL, Email);
		action.dropdownIndex(REGISTERTITLE, 3);   // drop down
		action.updateElement(REGISTERFIRSTNAME, FirstName);
		action.updateElement(REGISTERLASTNAME, Surname);		
		action.updateElement(REGPASSWORD, Password);
		action.updateElement(REGCHECKPASSWORD, ConfirmPassword);
		action.updateElement(REGISTERNECTARPOINTS, NectarCard);
		action.clickOnTheElement(COMPLETEREGISTER);
		
	}
	public void registerWithOutReCaptchaAssert() {
	Assert.assertEquals("Register with Tu", get.getElementText(ASSERTREGISTERWITHTU));
	Assert.assertEquals("Please check the recaptcha",
			driver.findElement(By.cssSelector("#reCaptcha-register")).getText());
	}
	public void clickHideShowButtonWhenRegister () throws InterruptedException {
		action.clickOnTheElement(REGISTERBUTTON);
		action.updateElement(REGISTEREMAIL, "veenagokula@gmail.com");
		action.dropdownIndex(REGISTERTITLE, 3);   // drop down
		Thread.sleep(3000);
		action.updateElement(REGISTERFIRSTNAME, "veena");
		action.updateElement(REGISTERLASTNAME, "gokula");		
		action.updateElement(REGPASSWORD, "man@1234");
		Thread.sleep(3000);
		action.clickOnTheElement(SHOW);
		
		
	}
	public void clickHideShowButtonWhenRegisterAssert () {
	Assert.assertEquals("Register with Tu", get.getElementText(ASSERTREGISTERWITHTU));
	}
	
	public void registerWithNectarCardDetails () {
		action.clickOnTheElement(REGISTERBUTTON);
		action.updateElement(REGISTEREMAIL, "veenagokula@gmail.com");
		action.dropdownIndex(REGISTERTITLE, 3);   // drop down
		action.updateElement(REGISTERFIRSTNAME, "veena");
		action.updateElement(REGISTERLASTNAME, "gokula");		
		action.updateElement(REGPASSWORD, "man@1234");
		action.updateElement(REGCHECKPASSWORD, "man@1234");
		action.updateElement(REGISTERNECTARPOINTS, "98765432");
		action.clickOnTheElement(COMPLETEREGISTER);
}
	public void registerWithNectarCardDetailsAssert() {
	Assert.assertEquals("Register with Tu", get.getElementText(ASSERTREGISTERWITHTU));
	System.out.println("please check recaptcha");
}
}
