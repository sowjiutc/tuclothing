package com.pages;

import org.junit.Assert;
import org.openqa.selenium.chrome.ChromeDriver;

import com.runner.BaseClass;

public class BasketPage extends BaseClass {

	
	public void userOnCheckOutPage() {
		System.setProperty("webdriver.chrome.driver", "C:\\Selenium-API\\ChromeDriver\\chromedriver.exe");
		driver = new ChromeDriver();
		driver.get(ProductConstants.BASE_URL);
		driver.manage().window().maximize();
		Assert.assertEquals("https://tuclothing.sainsburys.co.uk/", get.getCurrentUrl());
	}

	public void increaseDecreaseQuanityUpdate() throws InterruptedException {
		AddBasketUtility.productSelectActivities(ProductConstants.ITEMIMAGESELECTOR, 0);
		Thread.sleep(5000);
		action.clickOnTheElement(ProductConstants.VIEWBASKET);
		Thread.sleep(5000);
		action.clickOnTheElement(ProductConstants.CLICKVIEWBASKET);// clickview basket
		Thread.sleep(5000);// update quantity
		action.dropdownIndex(ProductConstants.QUANTITYUPDATEDROPDOWN, 2);

		Thread.sleep(5000);
		action.clickOnTheElement(ProductConstants.QUANTITYUPDATE); // increasing quantity
		// decrease quantity
		Thread.sleep(5000);
		action.dropdownIndex(ProductConstants.QUANTITYUPDATEDROPDOWN, 1);

	}

	public void increaseDecreaseQuanityUpdateAssert() {
		action.clickOnTheElement(ProductConstants.QUANTITYUPDATE);
		Assert.assertEquals("Product quantity has been updated.", get.getElementText(ProductConstants.UPDATEGLOBAL));
	}

	public void removeAllItemsFromBasketInCheckoutPage() throws InterruptedException {

		AddBasketUtility.productSelectActivities(ProductConstants.ITEMIMAGESELECTOR, 3);
		AddBasketUtility.productSelectActivities(ProductConstants.ITEMIMAGESELECTOR, 4);

		action.clickOnTheElement(ProductConstants.CHECKOUTBUTTON);
		Thread.sleep(3000);

	}

	public void removeAllItemsFromBasketInCheckoutPageAssert() {
		action.clickOnTheElement(ProductConstants.REMOVEPRODUCT);
		action.clickOnTheElement(ProductConstants.REMOVEPRODUCT);// 2 products removed

		Assert.assertEquals("Product has been removed from your cart.", get.getElementText(ProductConstants.UPDATEGLOBAL));
	}

	public void invalidPayVoucher() throws InterruptedException {// same for Validate the functionalities of valid
																	// voucher pay mode
		AddBasketUtility.productSelectActivities(ProductConstants.ITEMIMAGESELECTOR, 3);

		Thread.sleep(4000);
		action.clickOnTheElement(ProductConstants.CHECKOUTBUTTON);
		Thread.sleep(5000);
		action.clickOnTheElement(ProductConstants.SHOWPROMO);
		Thread.sleep(3000);
		action.clickOnTheElement(ProductConstants.VOUCHERPAY);
		Thread.sleep(3000);
		action.sendkeys(ProductConstants.VOUCHERPAY, "0000000");
	}

	public void invalidPayVoucherAssert() throws InterruptedException {
		Assert.assertEquals("https://tuclothing.sainsburys.co.uk/cart", get.getCurrentUrl());
		Thread.sleep(3000);
		action.clickOnTheElement(ProductConstants.SHOWPROMO);
	}

	public void checkoutWithItemsDecreased() throws InterruptedException {
		AddBasketUtility.productSelectActivities(ProductConstants.ITEMIMAGESELECTOR, 1);

		action.clickOnTheElement(ProductConstants.VIEWBASKET);
		Thread.sleep(4000);
		action.clickOnTheElement(ProductConstants.CLICKVIEWBASKET);
		Thread.sleep(5000);
		action.dropdownIndex(ProductConstants.QUANTITYUPDATEDROPDOWN, 1);
		Thread.sleep(3000);

	}

	public void checkoutWithItemsDecreasedAssert() {
		action.clickOnTheElement(ProductConstants.QUANTITYUPDATE);
		Assert.assertEquals("Product quantity has been updated.", get.getElementText(ProductConstants.UPDATEGLOBAL));
	}

	public void checkoutWithItemsIncreased() throws InterruptedException {
		AddBasketUtility.productSelectActivities(ProductConstants.ITEMIMAGESELECTOR, 1);
		Thread.sleep(5000);
		action.clickOnTheElement(ProductConstants.VIEWBASKET);
		Thread.sleep(4000);
		action.clickOnTheElement(ProductConstants.CLICKVIEWBASKET);// clickview basket
		Thread.sleep(5000);
		action.dropdownIndex(ProductConstants.QUANTITYUPDATEDROPDOWN, 2);
		Thread.sleep(2000);

	}

	public void checkoutWithItemsIncreasedAssert() {
		action.clickOnTheElement(ProductConstants.QUANTITYUPDATE);
		Assert.assertEquals("Product quantity has been updated.", get.getElementText(ProductConstants.UPDATEGLOBAL));
	}

	public void addingMaxNumberOfProducts() throws InterruptedException {

		AddBasketUtility.productSelectActivities(ProductConstants.ITEMIMAGESELECTOR, 4);
		AddBasketUtility.productSelectActivities(ProductConstants.ITEMIMAGESELECTOR, 3);
		AddBasketUtility.productSelectActivities(ProductConstants.ITEMIMAGESELECTOR, 6);

	}

	public void addingMaxNumberOfProductsAssert() throws InterruptedException {
		action.clickOnTheElement(ProductConstants.VIEWBASKET);
		Thread.sleep(4000);
		action.clickOnTheElement(ProductConstants.CLICKVIEWBASKET);// view basket and checkout
		Assert.assertEquals("6 items", get.getElementText(ProductConstants.CART_ITEMS_TEXT));
	}

	public void validateNavigationOfCheckout() throws InterruptedException {
		AddBasketUtility.productSelectActivities(ProductConstants.ITEMIMAGESELECTOR, 0);
		Thread.sleep(6000);
		action.clickOnTheElement(ProductConstants.CHECKOUTBUTTON);// check out button
	}

	public void validateNavigationOfCheckoutAssert() throws InterruptedException {
		Thread.sleep(4000);
		action.clickOnTheElement(ProductConstants.PROCEEDTOCHECKOUTUP);// proceed to check out button
		Assert.assertEquals("https://tuclothing.sainsburys.co.uk/login/checkout", get.getCurrentUrl());
	}

	public void checkoutMiniBasket() throws InterruptedException {
		AddBasketUtility.productSelectActivities(ProductConstants.ITEMIMAGESELECTOR, 4);
	}

	public void checkoutMiniBasketAssert() throws InterruptedException {
		Thread.sleep(6000);
		action.clickOnTheElement(ProductConstants.MINIBASKET); // minibasket
		Assert.assertEquals(
				"https://tuclothing.sainsburys.co.uk/p/Spot-Print-Tiered-Top/137093914-Mono?searchTerm=:newArrivals&searchProduct=",
				get.getCurrentUrl());

	}

}