package com.pages;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.chrome.ChromeDriver;

import com.runner.BaseClass;

import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class StoreLocatorPage extends BaseClass {

	private static String baseURL = "https://tuclothing.sainsburys.co.uk/";

	// Store Locator
	private static By STORELOCATORlINK = By.linkText("Tu Store Locator");
	private static By POSTCODEBOX = By.cssSelector("input[placeholder='Postcode or town']");
	private static By FINDSTORES = By.cssSelector("button[class='ln-c-button ln-c-button--primary']");
	private static By TOGGLEFULLSCREEN = By.cssSelector("button[title='Toggle fullscreen view']");
	private static By SATELLITE = By.cssSelector("div[title='Show satellite imagery']");
	private static By MAP = By.cssSelector("div[title='Show street map']");
	private static By CHILDREN = By.cssSelector("label[for='children']");
	private static By CLICKANDCOLLECT = By.cssSelector("label[for='click']");
	private static By MENS = By.cssSelector("label[for='men']");
	private static By WOMENS = By.cssSelector("label[for='women']");
	private static By ZOOMIN = By.cssSelector("button[title='Zoom in']");
	private static By ZOOMOUT = By.cssSelector("button[title='Zoom out']");
	private static By ASSERTH1 = By.cssSelector("h1");
	private static By ASSERTINVALIDPC=By.cssSelector(".ln-u-display-inline-block.ln-u-soft");
	private static By ASSERTCNC =By.cssSelector("#header1");

	public void verifyStoreLocatorPage() throws InterruptedException {
		System.setProperty("webdriver.chrome.driver", "C:\\Selenium-API\\ChromeDriver\\chromedriver.exe");
		driver = new ChromeDriver();
		driver.get(baseURL);
		driver.manage().window().maximize();
		Assert.assertEquals("https://tuclothing.sainsburys.co.uk/", get.getCurrentUrl());
		action.clickOnTheElement(STORELOCATORlINK);
		Thread.sleep(3000);
		Assert.assertEquals("Store Locator", get.getElementText(ASSERTH1));
	}
	public void invalidPostCode () {
	action.updateElement(POSTCODEBOX, "Tw0045");
	action.clickOnTheElement(FINDSTORES);
	
	}
	public void invalidPostCodeAssert() {
	Assert.assertEquals(
			"Sorry, no results found for your search. Please check your spelling and make sure you are searching for a town name or postcode.",
			get.getElementText(ASSERTINVALIDPC));
	System.out.println(
			"Sorry, no results found for your search. Please check your spelling and make sure you are searching for a town name or postcode.");
	}
	public void verifyBlankPostCode() {
		action.updateElement(POSTCODEBOX, "");
		action.clickOnTheElement(FINDSTORES);
	}
	public void verifyBlankPostCodeAssert() {
	System.out.println("Please fill Out deatils");
	Assert.assertEquals("Store Locator", driver.findElement(By.cssSelector("h1")).getText());
	}	
	public void toggleFullScreen() throws InterruptedException {
		action.updateElement(POSTCODEBOX, "Tw3 1TA");
		action.clickOnTheElement(FINDSTORES);
		Thread.sleep(3000);
	}
	public void toggleFullScreenOnMap() throws InterruptedException {
	Thread.sleep(3000);
	action.clickOnTheElement(TOGGLEFULLSCREEN);
	}
	public void toggleFullScreenOnMapAssert() {
	Assert.assertEquals("Store Locator", get.getElementText(ASSERTH1));
	}
	public void SwitchViewMapToSatellite () throws InterruptedException {
		action.updateElement(POSTCODEBOX, "Tw3 1TA");
		action.clickOnTheElement(FINDSTORES);
		Thread.sleep(3000);
		action.clickOnTheElement(SATELLITE);
		}
	public void SwitchViewMapToSatelliteAssert() throws InterruptedException {
	Thread.sleep(4000);
	action.clickOnTheElement(MAP);
	Assert.assertEquals("Store Locator", get.getElementText(ASSERTH1));
	}
	public void clickAndCollectChildrenWithValidPostcode() {
		action.updateElement(POSTCODEBOX, "Tw3 1TA");
		action.clickOnTheElement(FINDSTORES);
	}
	public void clickAndCollectChildren() throws InterruptedException {
	Thread.sleep(3000);
	action.clickOnTheElement(CHILDREN);
	Thread.sleep(3000);
	}
	public void clickAndCollectChildrenAssert() throws InterruptedException {
		action.clickOnTheElement(CLICKANDCOLLECT);
		Thread.sleep(3000);
		action.clickOnTheElement(FINDSTORES);
	Assert.assertEquals("Your nearest stores", get.getElementText(ASSERTCNC));
	}
	public void clickAndCollectWomen() throws InterruptedException {	
		
		action.updateElement(POSTCODEBOX, "Tw3 1TA");
		Thread.sleep(3000);
		action.clickOnTheElement(WOMENS);
	}
	public void clickAndCollectWomenAssert() throws InterruptedException {
		action.clickOnTheElement(CLICKANDCOLLECT);
		Thread.sleep(3000);
		action.clickOnTheElement(FINDSTORES);
		Assert.assertEquals("Your nearest stores", get.getElementText(ASSERTCNC));
	}
	public void clickAndCollectMen() throws InterruptedException {
		action.updateElement(POSTCODEBOX, "Tw3 1TA");
		Thread.sleep(3000);
		action.clickOnTheElement(MENS);
	}
	public void clickAndCollectMenAssert() {
		action.clickOnTheElement(FINDSTORES);
	Assert.assertEquals("Your nearest stores", get.getElementText(ASSERTCNC));
	}
	public void postcodeWithOutCheckBoxes() {
		action.updateElement(POSTCODEBOX, "Tw3 1TA");
		action.clickOnTheElement(FINDSTORES);
	}
	public void postcodeWithOutCheckBoxesAssert() {
		Assert.assertEquals("Your nearest stores", get.getElementText(ASSERTCNC));
	}
	public void storeLocatorNavigation() {
		action.clickOnTheElement(STORELOCATORlINK);
	}
	public void storeLocatorNavigationAssert() {
		Assert.assertEquals("Store Locator", get.getElementText(ASSERTH1));
	}
	public void validPostCodeCNC() {
		action.updateElement(POSTCODEBOX, "Tw3 1TA");
	}
	public void ValidPostCodeNearbyCNC() {
		action.clickOnTheElement(CLICKANDCOLLECT);
	}
	public void validPostCodeCNCAssert() {
		action.clickOnTheElement(FINDSTORES);
	Assert.assertEquals("Your nearest stores", get.getElementText(ASSERTCNC));
	}
	public void childrensClothStoreValidPostcode() {
		action.updateElement(POSTCODEBOX, "Tw3 1TA");
	}
	public void childrensClothStoreSearch() {
		action.clickOnTheElement(CHILDREN);
	}
	public void childrensClothStoresAssert() {
		action.clickOnTheElement(FINDSTORES);
		Assert.assertEquals("Your nearest stores", get.getElementText(ASSERTCNC));
	}
	public void mensclothingStore () {
		action.updateElement(POSTCODEBOX, "Tw3 1TA");
		action.clickOnTheElement(MENS);
	}
	public void mensClothinsAssert() {
		action.clickOnTheElement(FINDSTORES);
	Assert.assertEquals("Your nearest stores", get.getElementText(ASSERTCNC));
	}
	public void womensClothingStores() {
		action.updateElement(POSTCODEBOX, "Tw3 1TA");
		action.clickOnTheElement(WOMENS);
	}
	public void womensClothingStoresAssert() {
		action.clickOnTheElement(FINDSTORES);
		Assert.assertEquals("Your nearest stores", get.getElementText(ASSERTCNC));
	}
	public void zoomIN() {
		action.updateElement(POSTCODEBOX, "Tw3 1TA");
	}
	public void zoomInAssert() throws InterruptedException {
		action.clickOnTheElement(FINDSTORES);
		Thread.sleep(3000);
		action.clickOnTheElement(ZOOMIN);
		Assert.assertEquals("Store Locator", get.getElementText(ASSERTH1));
	}
	public void zoomOut() {
		action.updateElement(POSTCODEBOX, "Tw3 1TA");
}
	public void zoomOutAssert() throws InterruptedException {
		action.clickOnTheElement(FINDSTORES);
	Thread.sleep(3000);
	action.clickOnTheElement(ZOOMOUT);
	Assert.assertEquals("Store Locator", get.getElementText(ASSERTH1));
}
}
	