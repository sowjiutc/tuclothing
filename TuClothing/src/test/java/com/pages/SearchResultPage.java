package com.pages;

import org.junit.Assert;
import org.openqa.selenium.By;

import com.runner.BaseClass;

public class SearchResultPage extends BaseClass {
	
	private static By ASSERTH1 = By.cssSelector("h1");
	
	public void searchResultPageForValidProducts () {
	Assert.assertTrue(get.getElementText(ASSERTH1).toLowerCase().contains("jeans"));
	}
	public void searchResultForValidProductNumber() {
	Assert.assertTrue(get.getElementText(ASSERTH1).toLowerCase().contains("'137430191'"));	
	}
	public void searchResultForValidProductColour() {		
	Assert.assertTrue(get.getElementText(ASSERTH1).toLowerCase().contains("pink"));
	}
	public void searchWithSpecialAssert() {
	Assert.assertTrue(get.getElementText(ASSERTH1).contains("@\\!&"));
	}
	
}
