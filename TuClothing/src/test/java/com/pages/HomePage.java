package com.pages;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import com.runner.BaseClass;

public class HomePage extends BaseClass {
	
	private static String baseURL = "https://tuclothing.sainsburys.co.uk/";
	
			// Search
			private static By SEARCH = By.cssSelector("#search");
			private static By SEARCHBUTTON = By.cssSelector(".searchButton");
			private static By ERRORMESSAGEFORBLANKDATA=By.cssSelector("#search-empty-errors");
			private static By ASSERTH1 = By.cssSelector("h1");
			
			// Login
			private static By LOGINREGISTER = By.linkText("Tu Log In / Register");
			
			
	
	public void verifyHomePage() {
		
		System.setProperty("webdriver.chrome.driver", "C:\\Selenium-API\\ChromeDriver\\chromedriver.exe");
		driver = new ChromeDriver();
		driver.get(baseURL);
		driver.manage().window().maximize();
		Assert.assertEquals("https://tuclothing.sainsburys.co.uk/", get.getCurrentUrl());
		}
		public void searchForvalidProductName(String searchWord) {
		action.updateElement(SEARCH, searchWord);
		action.clickOnTheElement(SEARCHBUTTON);
		}
		
		public void searchForValidProductNumber() {
			action.updateElement(SEARCH, "137430191");
			action.clickOnTheElement(SEARCHBUTTON);
			}
		public void searchForValidProductColour() {
			action.updateElement(SEARCH, "pink");
			action.clickOnTheElement(SEARCHBUTTON);
		}
		public void searchWithBlank() {
			action.updateElement(SEARCH, " ");
			action.clickOnTheElement(SEARCHBUTTON);
		}
		public void searchWithBlankAssert() {
		Assert.assertEquals("Please complete a product search", get.getElementText(ERRORMESSAGEFORBLANKDATA));
		System.out.println("Please complete a product search");
		}
		public void searchWithAutoCompleteList () {
			action.updateElement(SEARCH, "dungarees");
		}
		public void searchWithAutoCompleteAssert() {
		Assert.assertEquals("https://tuclothing.sainsburys.co.uk/", get.getCurrentUrl());
		}
		public void searchWithSpecialCharecters() {
			action.updateElement(SEARCH, "@\\\\!&");
			action.clickOnTheElement(SEARCHBUTTON);
		}
		public void searchWithInValidData() {
			action.updateElement(SEARCH, "cheetos");
			action.clickOnTheElement(SEARCHBUTTON);
		}
		public void searchWithInValidAssert () {
		Assert.assertTrue(get.getElementText(ASSERTH1).contains("'cheetos'"));
		System.out.println("Sorry, no results for 'cheetos'");
		}
		public void validLoginCredentials() {
			action.clickOnTheElement(LOGINREGISTER);
		
		}
		public void loginNRegistrationLink() {
			action.clickOnTheElement(LOGINREGISTER);
		}
		
		
}
