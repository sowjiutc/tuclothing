package com.pages;

import org.junit.Assert;
import org.openqa.selenium.chrome.ChromeDriver;

import com.runner.BaseClass;

public class ProductDetailPage extends BaseClass {

	private static String baseURL = "https://tuclothing.sainsburys.co.uk/";

	

	public void userOnProductDetailPage() {
		System.setProperty("webdriver.chrome.driver", "C:\\Selenium-API\\ChromeDriver\\chromedriver.exe");
		driver = new ChromeDriver();
		driver.get(baseURL);
		driver.manage().window().maximize();
		Assert.assertEquals("https://tuclothing.sainsburys.co.uk/", get.getCurrentUrl());
	}

	public void addProductToBasket() throws InterruptedException {
		AddBasketUtility.productSelectActivities(ProductConstants.ITEMIMAGESELECTOR,2);
	}
	
	/*
	 * private static void productSelectActivities(By imageItem,int imageIndex)
	 * throws InterruptedException{ action.clickOnTheElement(WOMENLINK);
	 * Assert.assertEquals("Womens", get.getElementText(ASSERTWOMENS));
	 * Thread.sleep(3000); action.clickOnTheElement(NEWARRIVALS);
	 * Thread.sleep(3000); Assert.assertEquals("Womens Clothing New In",
	 * get.getElementText(ASSERTH1)); Thread.sleep(4000);
	 * action.clickOnTheElementWithIndex(imageItem, imageIndex); // First product of
	 * the page ITEMIMAGESELECTOR,2 Thread.sleep(3000);
	 * action.clickOnTheElementWithIndex(SIZECHECKBOX, 1); Thread.sleep(3000);
	 * action.dropdownIndex(PRODUCTQUANTITY, 2);
	 * action.clickOnTheElement(ADDTOCART); Thread.sleep(3000); }
	 */

	public void addProductToBasketAssert() throws InterruptedException {
		action.clickOnTheElement(ProductConstants.MINIBASKET); // minibasket
		Thread.sleep(3000);
		Assert.assertEquals("Qty:3", get.getElementText(ProductConstants.ASSERTPRODQUANTITY));
	}

	public void addProductToBasketWithOutLogin() throws InterruptedException {
		Thread.sleep(3000);
		AddBasketUtility.productSelectActivities(ProductConstants.ITEMIMAGESELECTOR,2);

	}

	public void addProductToBasketWithOutLoginAssert() throws InterruptedException {
		Thread.sleep(3000);
		action.clickOnTheElement(ProductConstants.MINIBASKET); // minibasket
		Thread.sleep(3000);
		Assert.assertEquals("Qty:3", get.getElementText(ProductConstants.ASSERTPRODQUANTITY));
	}

	public void multipleProductsWithOutLogin() throws InterruptedException {
		AddBasketUtility.productSelectActivities(ProductConstants.ITEMIMAGESELECTOR,1);
//		action.clickOnTheElement(WOMENLINK);
//		Assert.assertEquals("Womens", get.getElementText(ASSERTWOMENS));
//		Thread.sleep(3000);
//		action.clickOnTheElement(NEWARRIVALS);
//		Thread.sleep(3000);
//		Assert.assertEquals("Womens Clothing New In", get.getElementText(ASSERTH1));
//		Thread.sleep(4000);
//		action.clickOnTheElementWithIndex(ITEMIMAGESELECTOR, 1); // First product of the page
//		Thread.sleep(3000);
//		action.clickOnTheElementWithIndex(SIZECHECKBOX, 1);
//		Thread.sleep(3000);
//		action.dropdownIndex(PRODUCTQUANTITY, 2);
//		action.clickOnTheElement(ADDTOCART);
		Thread.sleep(3000);
		action.clickOnTheElement(ProductConstants.MINIBASKET);
		Thread.sleep(3000);
		action.clickOnTheElement(ProductConstants.MINIVIEWBASKET);
		Thread.sleep(3000);
		Assert.assertEquals("Qty:3", get.getElementText(ProductConstants.ASSERTPRODQUANTITY));
		
		AddBasketUtility.productSelectActivities(ProductConstants.ITEMIMAGESELECTOR,0);

		/*
		 * action.clickOnTheElement(WOMENLINK); Assert.assertEquals("Womens",
		 * get.getElementText(ASSERTWOMENS)); Thread.sleep(3000);
		 * action.clickOnTheElement(NEWARRIVALS); Thread.sleep(3000);
		 * Assert.assertEquals("Womens Clothing New In", get.getElementText(ASSERTH1));
		 * Thread.sleep(4000); action.clickOnTheElementWithIndex(ITEMIMAGESELECTOR, 0);
		 * // First product of the page Thread.sleep(3000);
		 * action.clickOnTheElementWithIndex(SIZECHECKBOX, 1); Thread.sleep(3000);
		 * action.dropdownIndex(PRODUCTQUANTITY, 1); Thread.sleep(5000);
		 * action.clickOnTheElement(ADDTOCART);
		 * Thread.sleep(3000);
		 */
		
	}

	public void multipleProductsWithOutLoginAssert() throws InterruptedException {
		action.clickOnTheElement(ProductConstants.MINIBASKET); // minibasket
		Thread.sleep(3000);
		action.clickOnTheElement(ProductConstants.MINIVIEWBASKET); // minibasket view basket
		Assert.assertEquals("Qty:2", get.getElementText(ProductConstants.ASSERTPRODQUANTITY));
	}

	// due to Recaptcha did not write for login
	public void multipleProductsWithLogin() throws InterruptedException {
		AddBasketUtility.productSelectActivities(ProductConstants.ITEMIMAGESELECTOR,1);
		/*
		 * action.clickOnTheElement(WOMENLINK); Assert.assertEquals("Womens",
		 * get.getElementText(ASSERTWOMENS)); Thread.sleep(3000);
		 * action.clickOnTheElement(NEWARRIVALS); Thread.sleep(3000);
		 * Assert.assertEquals("Womens Clothing New In", get.getElementText(ASSERTH1));
		 * Thread.sleep(4000); action.clickOnTheElementWithIndex(ITEMIMAGESELECTOR, 1);
		 * // First product of the page Thread.sleep(3000);
		 * action.clickOnTheElementWithIndex(SIZECHECKBOX, 1); Thread.sleep(3000);
		 * action.dropdownIndex(PRODUCTQUANTITY, 2);
		 * action.clickOnTheElement(ADDTOCART); Thread.sleep(3000);
		 */
		action.clickOnTheElement(ProductConstants.MINIBASKET);
		Thread.sleep(3000);
		action.clickOnTheElement(ProductConstants.MINIVIEWBASKET);
		Thread.sleep(3000);
		Assert.assertEquals("Qty:3", get.getElementText(ProductConstants.ASSERTPRODQUANTITY));

		AddBasketUtility.productSelectActivities(ProductConstants.ITEMIMAGESELECTOR,0);
		/*
		 * action.clickOnTheElement(WOMENLINK); Assert.assertEquals("Womens",
		 * get.getElementText(ASSERTWOMENS)); Thread.sleep(3000);
		 * action.clickOnTheElement(NEWARRIVALS); Thread.sleep(3000);
		 * Assert.assertEquals("Womens Clothing New In", get.getElementText(ASSERTH1));
		 * Thread.sleep(4000); action.clickOnTheElementWithIndex(ITEMIMAGESELECTOR, 0);
		 * // First product of the page Thread.sleep(3000);
		 * action.clickOnTheElementWithIndex(SIZECHECKBOX, 1); Thread.sleep(3000);
		 * action.dropdownIndex(PRODUCTQUANTITY, 1); Thread.sleep(5000);
		 * action.clickOnTheElement(ADDTOCART); Thread.sleep(3000);
		 */
	}

	public void multipleProductsWithLoginAssert() throws InterruptedException {
		action.clickOnTheElement(ProductConstants.MINIBASKET); // minibasket
		Thread.sleep(3000);
		action.clickOnTheElement(ProductConstants.MINIVIEWBASKET); // minibasket view basket
		Assert.assertEquals("Qty:2", get.getElementText(ProductConstants.ASSERTPRODQUANTITY));
	}

	public void guestRemovingItemsFromBasket() throws InterruptedException {
		AddBasketUtility.productSelectActivities(ProductConstants.ITEMIMAGESELECTOR,0);
		/*
		 * action.clickOnTheElement(WOMENLINK); Thread.sleep(3000);
		 * action.clickOnTheElement(NEWARRIVALS); Thread.sleep(3000);
		 * action.clickOnTheElementWithIndex(ITEMIMAGESELECTOR, 0); Thread.sleep(3000);
		 * action.clickOnTheElementWithIndex(SIZECHECKBOX, 1); Thread.sleep(4000);
		 * action.dropdownIndex(PRODUCTQUANTITY, 1);
		 * action.clickOnTheElement(ADDTOCART); Thread.sleep(3000);
		 */

	}

	public void guestRemovingItemsFromBasketAssert() throws InterruptedException {
		action.clickOnTheElement(ProductConstants.VIEWBASKET);
		Thread.sleep(5000);
		action.clickOnTheElement(ProductConstants.CLICKVIEWBASKET);// clickview basket
		Thread.sleep(7000);
		action.clickOnTheElement(ProductConstants.REMOVEPRODUCT);// remove product
		Thread.sleep(3000);
		action.clickOnTheElement(ProductConstants.BASKETEMPTY);
		driver.findElement(ProductConstants.BASKETEMPTY).click();
		Assert.assertEquals("No items in your basket", get.getElementText(ProductConstants.ADDCART));

	}

}
