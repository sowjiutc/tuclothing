Feature: Register

Scenario: Validate registration page of tu home page to registration page
Given User on home page
When User click on login/registration link
Then home page navigated to tu registration page

Scenario: Validate error message leaving all required fields blank 
Given User on home page 
And User click on login/registration link
When Mandatory fields left empty and tried to register
Then error displayed

Scenario: Verify the functionality without email address for registration 
Given User on home page 
And User click on login/registration link
When User tried to register with out email id
Then an assigned error message displayed

Scenario: Verify the functionality of invalid email address for registration
Given User on home page
And User click on login/registration link
When User tried to register invalid email id
Then User can see an assigned error message

Scenario: Verify the functionality of special characters or numbers in first name and sur name
Given User on home page
And User click on login/registration link
When User tried to register with special characters and numbers
Then User will be seeing a error message

Scenario: Verify the functionality of password and confirm password
Given User on home page
And User click on login/registration link
When password and confirm password are given different
Then error message popup password do not match

Scenario: Verify the length functionality of password
Given User on home page
And User click on login/registration link
When tried to register with minimum words
Then error message pop up 

Scenario: Validate the Nectar card which is not mandatory while registration
Given User on home page
And User click on login/registration link
When User nectar card not mandatory
Then User can register his email 

Scenario: Validate the Terms and conditions not accepted when registered
Given User on home page
And User click on login/registration link
When terms and conditions are not accepted 
Then User cannot register
@Smoke
Scenario: Validate the image captcha verification while registration
Given User on home page
And User click on login/registration link
When User enter valid credentials and image captcha button is not clicked
|email|sowji.g2@gmail.com|
|firstname|sowji|
|surname|gokula|
|password|12345|
|confirm Password|12345|
|nectarcard|98765432|




Then User cannot register with the given details

Scenario: Verify the functionality of show password option to click which is required
Given User on home page
And User click on login/registration link
When User tried to click on hide/show password
Then User should see the password which are in stars

Scenario: Validate  registration with nectar card details
Given User on home page
And User click on login/registration link
When User given all mandatory required details
Then User can register with his email id and nectar card details


