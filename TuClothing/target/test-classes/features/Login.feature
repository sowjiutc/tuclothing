Feature: Login functionality

Scenario: Validate the functionality of tu clothing login page
Given User on home page
When User click on login/register button
And User enter valid credentials
|email|veena@test.com|
|password|123456|
Then User should able to see welcome message

Scenario:  Validate the functionality of valid email id and invalid password of tu clothing login page
Given User on home page
When User click on login/register button
And User provides invalid email id and password
Then User should not able to login successfully with valid email id and invalid password

Scenario:  Validate the functionality of blank email id and password of tu clothing login page
Given User on home page
When User click on login/register button
And User provides blank email id and password
Then User should see error popup

Scenario:  Verify the login functionality with invalid Id and valid password
Given User on home page
When User click on login/register button
And User give invalid ID and valid password
Then User should get a error message with invalid Id and valid password

Scenario:  Verify the login functionality with invalid Id and invalid password
Given User on home page
When User click on login/register button
And User give invalid ID and Invalid password
Then User should get a error message please check the fields below and try again

Scenario:  Verify the functionality of show/hide password
Given User on home page
When User click on login/register button
And User give valid credentials and click on hide or show password
Then User should see the password which are in bullet points

Scenario:  Verify the functionality of back button to click after logout
Given User on home page
When User click on login/register button
When User login with valid credentials and logout 
Then User should not be able to login back when click back button 

Scenario:  Verify the  functionality of login credentials without recaptcha
Given User on home page
When User click on login/register button
And User given valid credentials with out recaptcha
Then User should not be able to log in with out recaptcha




